﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.Security.Authentication.Web;
using AsyncOAuth;
using CentPx.Client;
using CentPx.Client.Model;
using Cimbalino.Toolkit.Extensions;
using Cimbalino.Toolkit.Services;
using Newtonsoft.Json;

namespace CentPx.Core.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private const string AccessTokenKey = "AccessToken";
        private const string UserKey = "User";
        private const string CallbackUrl = "http://ferretlabs.com/centpx";

        private readonly ICentPxClient _centPxClient;
        private readonly IApplicationSettingsServiceHandler _settingsService;
        public AuthenticationService(ICentPxClient centPxClient, IApplicationSettingsService settingsService)
        {
            _centPxClient = centPxClient;
            _settingsService = settingsService.Roaming;

            CheckForAccessToken();
        }

        public event EventHandler<LoginStatusChangedArgs> LoginStatusChanged;
        public bool IsSignedIn { get; private set; }
        public User SignedInUser { get; private set; }

        private void CheckForAccessToken()
        {
            var json = _settingsService.Get(AccessTokenKey, string.Empty);
            var accessToken = JsonConvert.DeserializeObject<AccessToken>(json);

            SaveAccessToken(accessToken);

            var userJson = _settingsService.Get(UserKey, string.Empty);
            var user = JsonConvert.DeserializeObject<User>(userJson);

            SetUser(user);
        }

        private void SetUser(User user)
        {
            if (user != null)
            {
                var json = JsonConvert.SerializeObject(user);
                _settingsService.Set(UserKey, json);
            }

            SignedInUser = user;
            IsSignedIn = SignedInUser != null;
        }

        public async Task Authenticate()
        {
            var url = await _centPxClient.GetAuthorisationUrl();

            var result = await WebAuthenticationBroker.AuthenticateAsync(WebAuthenticationOptions.None, new Uri(url, UriKind.Absolute), new Uri(CallbackUrl, UriKind.Absolute));
            if (result.ResponseStatus == WebAuthenticationStatus.Success)
            {
                var returnedUrl = new Uri(result.ResponseData);
                var query = returnedUrl.QueryString();
                var code = query.FirstOrDefault(x => x.Key == "oauth_verifier");
                if (string.IsNullOrEmpty(code.Value))
                {
                    return;
                }

                var accessToken = await _centPxClient.SwapForAccessToken(code.Value);
                SaveAccessToken(accessToken);

                var user = await _centPxClient.GetSignedInUser();
                SetUser(user);

                SetLoginStatusChanged();
            }
        }

        public void SignOut()
        {
            _settingsService.Remove(AccessTokenKey);
            _settingsService.Remove(UserKey);
            SignedInUser = null;
            IsSignedIn = false;

            SetLoginStatusChanged();
        }

        private void SaveAccessToken(AccessToken accessToken)
        {
            var json = JsonConvert.SerializeObject(accessToken);
            _settingsService.Set(AccessTokenKey, json);

            _centPxClient.SetAccessToken(accessToken);
        }

        private void SetLoginStatusChanged()
        {
            LoginStatusChanged?.Invoke(this, new LoginStatusChangedArgs(IsSignedIn, SignedInUser));
        }
    }
}
