using System;
using CentPx.Client.Model;

namespace CentPx.Core.Services
{
    public class LoginStatusChangedArgs : EventArgs
    {
        public bool IsSignedIn { get; set; }
        public User User { get; set; }

        public LoginStatusChangedArgs(bool isSignedIn)
            : this(isSignedIn, null)
        {
        }

        public LoginStatusChangedArgs(bool isSignedIn, User user)
        {
            IsSignedIn = isSignedIn;
            User = user;
        }
    }
}