using System;
using System.Threading.Tasks;
using CentPx.Client.Model;

namespace CentPx.Core.Services
{
    public interface IAuthenticationService
    {
        event EventHandler<LoginStatusChangedArgs> LoginStatusChanged;
        bool IsSignedIn { get; }
        User SignedInUser { get; }
        Task Authenticate();
        void SignOut();
    }
}