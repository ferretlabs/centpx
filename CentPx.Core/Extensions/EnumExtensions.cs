using System;
using System.Collections.Generic;
using System.Linq;

namespace CentPx.Core.Extensions
{
    public static class EnumExtensions
    {
        public static List<T> ToList<T>(this Array array)
        {
            return (from object item in array select (T)item).ToList();
        }
    }
}