using System;
using System.Globalization;

namespace CentPx.Core.Extensions
{
    public static class DateTimeOffsetExtensions
    {
        public static string ToRelativeTime(this DateTimeOffset dateTime)
        {
            var ts = DateTimeOffset.UtcNow - dateTime;

            if (ts.TotalMinutes < 1.5)
            {
                return "just now";
            }
            int v = (int)Math.Round(ts.TotalMinutes);
            if (ts.TotalMinutes < 60)
            {
                return v + " minutes ago";
            }
            v = (int)Math.Round(ts.TotalHours);
            if (v == 1)
            {
                return "1 hour ago";
            }
            if (ts.TotalHours <= 24)
            {
                return v + " hours ago";
            }
            v = (int)Math.Round(ts.TotalDays);
            if (ts.TotalDays < 7)
            {
                if (v <= 1)
                {
                    return v + " day ago";
                }
                else
                {
                    return v + " days ago";
                }
            }
            if (ts.TotalDays < 14)
            {
                return "1 week ago";
            }
            if (ts.TotalDays < 31)
            {
                int wkCount = (int)Math.Round(ts.TotalDays / 7);
                return wkCount + " weeks ago";
            }
            if (v < 365)
            {
                return dateTime.ToString("m");
            }

            // foursquare iOS uses this sort of format: "Fri Sep 10"
            return dateTime.ToString("ddd MMM d");
        }

        public static string ToShortDate(this DateTimeOffset dateTime)
        {
            return dateTime.ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern);
        }
    }
}