﻿using System;
using Windows.Foundation;

namespace CentPx.Core.Extensions
{
    public static class AsyncActionExtensions
    {
        public static void DontAwait(this IAsyncAction task, string reasonForNotAwaiting)
        {
            if (string.IsNullOrEmpty(reasonForNotAwaiting))
            {
                throw new ArgumentNullException(nameof(reasonForNotAwaiting), "Give a reason!!!!!");
            }
        }
    }
}