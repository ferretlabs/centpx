﻿using System;
using System.Collections.Generic;
using CentPx.Core.Extensions;

namespace CentPx.Core.Helpers
{
    public class EnumHelper
    {
        public static List<T> GetValues<T>()
            where T : struct
        {
            return Enum.GetValues(typeof(T)).ToList<T>();
        }
    }
}
