﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    public class GalleriesResponse
    {
        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }

        [JsonProperty("total_pages")]
        public int TotalPages { get; set; }

        [JsonProperty("total_items")]
        public int TotalItems { get; set; }

        [JsonProperty("galleries")]
        public List<Gallery> Galleries { get; set; }

        [JsonProperty("filters")]
        public Filters Filters { get; set; }
    }
}
