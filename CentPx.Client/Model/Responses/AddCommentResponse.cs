using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    public class AddCommentResponse : StatusResponse
    {
        [JsonProperty("comment")]
        public Comment Comment { get; set; }
    }
}