﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    public class UserSearchResponse : BaseItemsResponse
    {
        [JsonProperty("users")]
        public List<User> Users { get; set; }
    }
}
