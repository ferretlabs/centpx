using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    internal class BooleanResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
    }
}