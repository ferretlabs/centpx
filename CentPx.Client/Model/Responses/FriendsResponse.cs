﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    public class FriendsResponse
    {
        [JsonProperty("friends_count")]
        public int FriendsCount { get; set; }

        [JsonProperty("friends_pages")]
        public int FriendsPages { get; set; }

        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("friends")]
        public List<User> Friends { get; set; }
    }
}
