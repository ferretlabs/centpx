﻿using System.Net;
using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    public class StatusResponse
    {
        [JsonProperty("status")]
        public HttpStatusCode Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
