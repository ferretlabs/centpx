﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    public class CommentsResponse
    {
        [JsonProperty("media_type")]
        public string MediaType { get; set; }

        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }

        [JsonProperty("total_pages")]
        public int TotalPages { get; set; }

        [JsonProperty("total_items")]
        public int TotalItems { get; set; }

        [JsonProperty("comments")]
        public List<Comment> Comments { get; set; }
    }
}
