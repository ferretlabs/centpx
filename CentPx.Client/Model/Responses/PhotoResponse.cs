﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    public class EditoredBy
    {
    }

    public class PhotoResponse
    {

        [JsonProperty("photo")]
        public Photo Photo { get; set; }

        [JsonProperty("comments")]
        public List<Comment> Comments { get; set; }
    }
}
