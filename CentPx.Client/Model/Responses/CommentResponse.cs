using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    internal class CommentResponse : StatusResponse
    {
        [JsonProperty("comment")]
        public Comment Comment { get; set; }
    }
}