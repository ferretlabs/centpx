﻿using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    internal class ShareUrlResponse
    {
        [JsonProperty("share_url")]
        public string ShareUrl { get; set; }
    }
}