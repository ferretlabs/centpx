using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    internal class GalleryResponse : StatusResponse
    {
        [JsonProperty("gallery")]
        public Gallery Gallery { get; set; }
    }
}