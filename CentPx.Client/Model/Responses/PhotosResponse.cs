﻿using System.Collections.Generic;
using CentPx.Client.Converters;
using CentPx.Client.Model.Types;
using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    public class PhotosResponse : BaseItemsResponse
    {
        [JsonProperty("photos")]
        public List<Photo> Photos { get; set; }

        [JsonProperty("filters")]
        public Filters Filters { get; set; }

        [JsonProperty("feature")]
        [JsonConverter(typeof(FeatureConverter))]
        public Feature Feature { get; set; }
    }
}
