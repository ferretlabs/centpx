﻿using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    internal class UserResponse
    {
        [JsonProperty("user")]
        public User User { get; set; }
    }
}