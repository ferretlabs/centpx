﻿using Newtonsoft.Json;

namespace CentPx.Client.Model.Responses
{
    public abstract class BaseItemsResponse
    {
        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }

        [JsonProperty("total_pages")]
        public int TotalPages { get; set; }

        [JsonProperty("total_items")]
        public int TotalItems { get; set; }
    }
}