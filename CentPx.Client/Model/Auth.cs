﻿using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class Auth
    {
        [JsonProperty("facebook")]
        public int Facebook { get; set; }

        [JsonProperty("twitter")]
        public int Twitter { get; set; }

        [JsonProperty("google_oauth2")]
        public int GoogleOauth2 { get; set; }
    }
}