﻿using CentPx.Client.Attributes;

namespace CentPx.Client.Model.Types
{
    public enum Feature
    {
        [FeatureInfo("popular", "Popular")]
        Popular,
        [FeatureInfo("highest_rated", "Highest rated")]
        HighestRated,
        [FeatureInfo("upcoming", "Upcoming")]
        Upcoming,
        [FeatureInfo("editors", "Editor's choice")]
        Editors,
        [FeatureInfo("fresh_today", "Fresh today")]
        FreshToday,
        [FeatureInfo("fresh_yesterday", "Fresh yesterday")]
        FreshYesterday,
        [FeatureInfo("fresh_week", "Fresh this week")]
        FreshThisWeek,
        [FeatureInfo("user", "User")]
        User,
        [FeatureInfo("user_friends", "Followed users")]
        FollowedUsers,
        [FeatureInfo("user_favorites", "Favourites")]
        Favorites
    }
}