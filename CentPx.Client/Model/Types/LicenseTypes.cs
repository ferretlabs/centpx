﻿using CentPx.Client.Attributes;

namespace CentPx.Client.Model.Types
{
    public enum LicenseTypes
    {
        [Description("Standard 500px License")]
        Standard = 0,
        [Description("Creative Commons License Non Commercial Attribution")]
        NonCommercialAttribution = 1,
        [Description("Creative Commons License Non Commercial No Derivatives")]
        NonCommercialNoDerivatives = 2,
        [Description("Creative Commons License Non Commercial Share Alike")]
        NonCommercialShareAlike = 3,
        [Description("Creative Commons License Attribution")]
        Attribution = 4,
        [Description("Creative Commons License No Derivatives")]
        NoDerivatives = 5,
        [Description("Creative Commons License Share Alike")]
        ShareAlike = 6,
        [Description("Creative Commons License Public Domain Mark 1.0")]
        PublicDomain1 = 7,
        [Description("Creative Commons License Public Domain Dedication")]
        PublicDomainDedication = 8
    }
}