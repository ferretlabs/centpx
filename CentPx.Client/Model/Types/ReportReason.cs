namespace CentPx.Client.Model.Types
{
    public enum ReportReason
    {
        Offensive = 1,
        Spam = 2,
        Offtopic = 3,
        Copyright = 4,
        WrongContent = 5,
        IsAdult = 6, 
        Other = 0
    }
}