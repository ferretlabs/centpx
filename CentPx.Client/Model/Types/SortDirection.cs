﻿using CentPx.Client.Attributes;

namespace CentPx.Client.Model.Types
{
    public enum SortDirection
    {
        [Description("asc")]
        Ascending,
        [Description("desc")]
        Descending
    }
}