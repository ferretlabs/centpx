﻿using CentPx.Client.Attributes;

namespace CentPx.Client.Model.Types
{
    public enum ImageSize
    {
        [ImageProperties(70, "70px x 70px", Cropped.Cropped)]
        Square70 = 1,
        [ImageProperties(140, "140px x 140px", Cropped.Cropped)]
        Square140 = 2,
        [ImageProperties(280, "280px x 280px", Cropped.Cropped)]
        Square280 = 3,
        [ImageProperties(100, "100px x 100px", Cropped.Cropped)]
        Square100 = 100,
        [ImageProperties(200, "200px x 200px", Cropped.Cropped)]
        Square200 = 200,
        [ImageProperties(440, "440px x 440px", Cropped.Cropped)]
        Square440 = 440,
        [ImageProperties(600, "600px x 600px", Cropped.Cropped)]
        Square600 = 600,

        [ImageProperties(900, "900px on the longest edge", Cropped.Uncropped)]
        Long900 = 4,
        [ImageProperties(1170, "1170px on the longest edge", Cropped.Uncropped)]
        Long1170 = 5,
        [ImageProperties(1080, "1080px high", Cropped.Uncropped)]
        High1080 = 6,
        [ImageProperties(300, "300px high", Cropped.Uncropped)]
        High300 = 20,
        [ImageProperties(600, "600px high", Cropped.Uncropped)]
        High600 = 21,
        [ImageProperties(256, "256px on the longest edge", Cropped.Uncropped)]
        Long256 = 30,
        [ImageProperties(450, "450px high", Cropped.Uncropped)]
        High450 = 31,
        [ImageProperties(1080, "1080px on the longest edge", Cropped.Uncropped)]
        Long1080 = 1080,
        [ImageProperties(1600, "1600px on the longest edge", Cropped.Uncropped)]
        Long1600 = 1600,
        [ImageProperties(2048, "2048px on the longest edge", Cropped.Uncropped)]
        Long2048 = 2048
    }

    public enum Cropped
    {
        Cropped,
        Uncropped
    }
}
