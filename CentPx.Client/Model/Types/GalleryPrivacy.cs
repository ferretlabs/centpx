﻿namespace CentPx.Client.Model.Types
{
    public enum GalleryPrivacy
    {
        Private,
        Public,
        Both
    }
}