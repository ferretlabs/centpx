﻿using CentPx.Client.Attributes;

namespace CentPx.Client.Model.Types
{
    public enum SortBy
    {
        [SortBy("created_at", "Created at")]
        CreatedAt,
        [SortBy("rating", "Rating")]
        Rating,
        [SortBy("highest_rating", "Highest rating")]
        HighestRating,
        [SortBy("times_viewed", "Times viewed")]
        TimesViewed,
        [SortBy("votes_count", "Vote count")]
        VotesCount,
        [SortBy("comments_count", "Comment count")]
        CommentsCount,
        [SortBy("taken_at", "Taken at")]
        TakenAt,
        [SortBy("_score", "Score")]
        Score,
        [SortBy("last_added_to_at", "Last added to")]
        LastAddedTo,
        [SortBy("position", "Position")]
        Position
    }
}