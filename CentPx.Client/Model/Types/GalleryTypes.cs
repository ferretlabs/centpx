using CentPx.Client.Attributes;

namespace CentPx.Client.Model.Types
{
    public enum GalleryTypes
    {
        [Description("Any photo on 500px")]
        General,
        [Description("Marketplace photos")]
        Lightbox,
        [Description("Photos display on the portfolio page")]
        Portfolio,
        [Description("Photos uploaded by the gallery owner")]
        Profile,
        [Description("Photos favorited by the gallery owner via the old API")]
        Favorite
    }
}