﻿using CentPx.Client.Model.Types;
using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class ImageInfo
    {
        [JsonProperty("size")]
        public ImageSize Size { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("https_url")]
        public string HttpsUrl { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }
    }
}