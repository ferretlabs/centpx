﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class Comment
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("user_id")]
        public int UserId { get; set; }

        [JsonProperty("to_whom_user_id")]
        public int PhotoOwnerId { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("parent_id")]
        public int? ParentId { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("flagged")]
        public bool Flagged { get; set; }

        [JsonProperty("rating")]
        public int Rating { get; set; }

        [JsonProperty("replies")]
        public List<Comment> Replies { get; set; }

        [JsonProperty("voted")]
        public bool Voted { get; set; }
    }
}