﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    [DebuggerDisplay("Username: {Username}")]
    public class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("firstname")]
        public string Firstname { get; set; }

        [JsonProperty("lastname")]
        public string Lastname { get; set; }

        [JsonProperty("birthday")]
        public DateTimeOffset? Birthday { get; set; }

        [JsonProperty("following")]
        public bool Following { get; set; }

        [JsonProperty("sex")]
        public Sex? Sex { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("registration_date")]
        public DateTimeOffset? RegistrationDate { get; set; }

        [JsonProperty("about")]
        public string About { get; set; }

        [JsonProperty("usertype")]
        public int Usertype { get; set; }

        [JsonProperty("domain")]
        public string Domain { get; set; }

        [JsonProperty("fotomoto_on")]
        public bool FotomotoOn { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }

        [JsonProperty("show_nude")]
        public bool ShowNude { get; set; }

        [JsonProperty("allow_sale_requests")]
        public int AllowSaleRequests { get; set; }

        [JsonProperty("fullname")]
        public string Fullname { get; set; }

        [JsonProperty("userpic_url")]
        public string UserpicUrl { get; set; }

        [JsonProperty("userpic_https_url")]
        public string UserpicHttpsUrl { get; set; }

        [JsonProperty("cover_url")]
        public string CoverUrl { get; set; }

        [JsonProperty("upgrade_status")]
        public AccountStatusTypes UpgradeStatus { get; set; }

        [JsonProperty("store_on")]
        public bool StoreOn { get; set; }

        [JsonProperty("photos_count")]
        public int PhotosCount { get; set; }

        [JsonProperty("galleries_count")]
        public int GalleriesCount { get; set; }

        [JsonProperty("affection")]
        public int Affection { get; set; }

        [JsonProperty("in_favorites_count")]
        public int InFavoritesCount { get; set; }

        [JsonProperty("friends_count")]
        public int FriendsCount { get; set; }

        [JsonProperty("followers_count")]
        public int FollowersCount { get; set; }

        [JsonProperty("analytics_code")]
        public string AnalyticsCode { get; set; }

        [JsonProperty("invite_pending")]
        public bool InvitePending { get; set; }

        [JsonProperty("invite_accepted")]
        public bool InviteAccepted { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("shadow_email")]
        public string ShadowEmail { get; set; }

        [JsonProperty("upload_limit")]
        public int? UploadLimit { get; set; }

        [JsonProperty("upload_limit_expiry")]
        public DateTimeOffset? UploadLimitExpiry { get; set; }

        [JsonProperty("upgrade_type")]
        public int UpgradeType { get; set; }

        [JsonProperty("upgrade_status_expiry")]
        public DateTimeOffset? UpgradeStatusExpiry { get; set; }

        [JsonProperty("auth")]
        public Auth Auth { get; set; }

        [JsonProperty("presubmit_for_licensing")]
        public object PresubmitForLicensing { get; set; }

        [JsonProperty("contacts")]
        public Contacts Contacts { get; set; }

        [JsonProperty("equipment")]
        public Equipment Equipment { get; set; }

        [JsonProperty("avatars")]
        public Avatars Avatars { get; set; }
    }
}
