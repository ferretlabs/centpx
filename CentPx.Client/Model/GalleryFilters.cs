using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class GalleryFilters
    {
        [JsonProperty("privacy")]
        public string Privacy { get; set; }

        [JsonProperty("kinds")]
        public bool Kinds { get; set; }
    }
}