﻿using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class AvatarImage
    {
        [JsonProperty("http")]
        public string Http { get; set; }

        [JsonProperty("https")]
        public string Https { get; set; }
    }
}