﻿using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class Contacts
    {
        [JsonProperty("twitter")]
        public string Twitter { get; set; }

        [JsonProperty("facebookpage")]
        public string Facebookpage { get; set; }
    }
}