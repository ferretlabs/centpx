﻿using CentPx.Client.Attributes;

namespace CentPx.Client.Model
{
    public enum CategoryTypes
    {
        Uncategorized = 0,
        Abstract = 10,
        Animals = 11,
        [Description("Black and White")]
        BlackAndWhite = 5,
        Celebrities = 1,
        [Description("City and Architecture")]
        CityAndArchitecture = 9,
        Commercial = 15,
        Concert = 16,
        Family = 20,
        Fashion = 14,
        Film = 2,
        FineArt = 24,
        Food = 23,
        Journalism = 3,
        Landscapes = 8,
        Macro = 12,
        Nature = 18,
        Nude = 4,
        People = 7,
        [Description("Performing Arts")]
        PerformingArts = 19,
        Sport = 17,
        [Description("Still Life")]
        StillLife = 6,
        Street = 21,
        Transportation = 26,
        Travel = 13,
        Underwater = 22,
        [Description("Urban Exploration")]
        UrbanExploration = 27,
        Wedding = 25
    }
}