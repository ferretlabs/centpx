﻿namespace CentPx.Client.Model
{
    public enum Sex
    {
        Unspecified = 0,
        Male = 1, 
        Female = 2
    }
}