﻿using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class Avatars
    {
        [JsonProperty("default")]
        public AvatarImage Default { get; set; }

        [JsonProperty("large")]
        public AvatarImage Large { get; set; }

        [JsonProperty("small")]
        public AvatarImage Small { get; set; }

        [JsonProperty("tiny")]
        public AvatarImage Tiny { get; set; }
    }
}