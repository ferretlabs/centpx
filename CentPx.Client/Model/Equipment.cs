using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class Equipment
    {
        [JsonProperty("camera")]
        public string[] Camera { get; set; }
    }
}