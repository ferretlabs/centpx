﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class UsersResponse
    {
        [JsonProperty("current_page")]
        public int CurrentPage { get; set; }

        [JsonProperty("total_pages")]
        public int TotalPages { get; set; }

        [JsonProperty("total_items")]
        public int TotalItems { get; set; }

        [JsonProperty("users")]
        public List<User> Users { get; set; }
    }
}
