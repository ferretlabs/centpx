using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class Filters
    {
        [JsonProperty("category")]
        public bool Category { get; set; }

        [JsonProperty("exclude")]
        public bool Exclude { get; set; }

        [JsonProperty("user_id")]
        public int UserId { get; set; }
    }
}