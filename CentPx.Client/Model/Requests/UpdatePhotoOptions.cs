﻿using System.Collections.Generic;
using CentPx.Client.Model.Types;

namespace CentPx.Client.Model.Requests
{
    public class UpdatePhotoOptions : BasePhotoOptions
    {
        public List<string> TagsToAdd { get; set; }
        public List<string> TagsToRemove { get; set; }
        public bool? IsNsfw { get; set; }
        public LicenseTypes? License { get; set; }
    }
}
