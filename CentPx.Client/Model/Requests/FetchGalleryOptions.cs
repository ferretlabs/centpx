﻿using System.Collections.Generic;
using CentPx.Client.Model.Types;

namespace CentPx.Client.Model.Requests
{
    public class FetchGalleryOptions
    {
        public SortBy? SortBy { get; set; }
        public ImageSize? CoverImageSize { get; set; }
        public bool? IncludeCover { get; set; }
        public SortDirection? SortDirection { get; set; }
        public GalleryPrivacy? GalleryPrivacy { get; set; }
        public List<GalleryTypes> GalleryTypes { get; set; }
        internal int? Page { get; set; }
        internal int? ResultsPerPage { get; set; }
    }
}