﻿using CentPx.Client.Model.Types;

namespace CentPx.Client.Model.Requests
{
    public class FetchPhotoOptions : BaseFetchPhotoOptions
    {
        public SortDirection? SortDirection { get; set; }
        public bool? IncludeMarketplace { get; set; }
        internal Feature? Feature { get; set; }
    }
}