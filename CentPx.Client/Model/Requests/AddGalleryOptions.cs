﻿using CentPx.Client.Model.Types;

namespace CentPx.Client.Model.Requests
{
    public class AddGalleryOptions : BaseGalleryOptions
    {
        public GalleryTypes GalleryType { get; set; }
    }
}