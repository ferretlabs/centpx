﻿using System.Collections.Generic;

namespace CentPx.Client.Model.Requests
{
    public abstract class BasePhotoOptions
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public CategoryTypes? Category { get; set; }
        public List<string> Tags { get; set; }
        public string ShutterSpeed { get; set; }
        public string FocalLength { get; set; }
        public string Aperture { get; set; }
        public int? Iso { get; set; }
        public string Camera { get; set; }
        public string Lens { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool? IsPrivate { get; set; }
    }
}