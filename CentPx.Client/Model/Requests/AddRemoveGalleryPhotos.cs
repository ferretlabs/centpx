﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CentPx.Client.Model.Requests
{
    internal class After
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }

    internal class Add
    {
        [JsonProperty("after")]
        public After After { get; set; }

        [JsonProperty("photos")]
        public List<int> Photos { get; set; }
    }

    internal class Remove
    {
        [JsonProperty("photos")]
        public List<int> Photos { get; set; }
    }

    internal class AddRemoveGalleryPhotos
    {
        [JsonProperty("add")]
        public Add Add { get; set; }

        [JsonProperty("remove")]
        public Remove Remove { get; set; }
    }
}
