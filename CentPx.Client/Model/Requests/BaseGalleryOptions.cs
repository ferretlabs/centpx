namespace CentPx.Client.Model.Requests
{
    public abstract class BaseGalleryOptions
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Subtitle { get; set; }
        public bool? IsPrivate { get; set; }
        public int? CoverPhotoId { get; set; }
        public string CustomPath { get; set; }
    }
}