﻿using CentPx.Client.Attributes;
using CentPx.Client.Extensions;

namespace CentPx.Client.Model.Requests
{
    public class GeoLocation
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public double Radius { get; set; }
        public RadiusUnits RadiusUnits { get; set; }

        public GeoLocation(double longitude, double latitude, double radius, RadiusUnits radiusUnits)
        {
            Longitude = longitude;
            Latitude = latitude;
            Radius = radius;
            RadiusUnits = radiusUnits;
        }

        public override string ToString()
        {
            return $"{Latitude},{Longitude},{Radius}<{RadiusUnits.GetDescription()}>";
        }
    }

    public enum RadiusUnits
    {
        [Description("km")]
        Kilometer,
        [Description("mi")]
        Mile
    }
}
