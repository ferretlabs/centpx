﻿namespace CentPx.Client.Model.Requests
{
    public class FetchGalleryPhotoOptions : FetchPhotoOptions
    {
        public bool? IncludeMissingPhotos { get; set; }
        public bool? IncludeGeoLocation { get; set; }
        public bool? IncludeLicensing { get; set; }
    }
}