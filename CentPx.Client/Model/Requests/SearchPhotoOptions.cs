using System.Collections.Generic;
using CentPx.Client.Model.Types;

namespace CentPx.Client.Model.Requests
{
    public class SearchFetchPhotoOptions : BaseFetchPhotoOptions
    {
        public List<LicenseTypes> LicenseTypes { get; set; }
        public GeoLocation GeoLocation { get; set; }
    }
}