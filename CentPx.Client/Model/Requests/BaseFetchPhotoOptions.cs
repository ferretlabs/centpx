﻿using System.Collections.Generic;
using CentPx.Client.Model.Types;

namespace CentPx.Client.Model.Requests
{
    public abstract class BaseFetchPhotoOptions
    {
        public List<CategoryTypes> Categories { get; set; }
        public List<CategoryTypes> ExcludeCategories { get; set; }
        public SortBy? SortBy { get; set; }
        public bool? IncludeTags { get; set; }
        internal int? Page { get; set; }
        internal int? ResultsPerPage { get; set; }
        public List<ImageSize> ImageSizes { get; set; }
    }
}