using System;
using System.Collections.Generic;
using CentPx.Client.Model.Types;
using Newtonsoft.Json;

namespace CentPx.Client.Model
{
    public class Gallery
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("privacy")]
        public bool Privacy { get; set; }

        [JsonProperty("kind")]
        public GalleryTypes Kind { get; set; }

        [JsonProperty("user_id")]
        public int UserId { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset? CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset? UpdatedAt { get; set; }

        [JsonProperty("items_count")]
        public int ItemsCount { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("nsfw")]
        public bool Nsfw { get; set; }

        [JsonProperty("thumbnail_photos")]
        public List<Photo> ThumbnailPhotos { get; set; }

        [JsonProperty("custom_path")]
        public string CustomPath { get; set; }
    }
}