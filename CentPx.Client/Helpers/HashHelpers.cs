﻿namespace CentPx.Client.Helpers
{
    internal class HashHelpers
    {
        public static byte[] ComputeHash(byte[] key, byte[] buffer)
        {
            var crypt = Windows.Security.Cryptography.Core.MacAlgorithmProvider.OpenAlgorithm("HMAC_SHA1");
            var keyBuffer = Windows.Security.Cryptography.CryptographicBuffer.CreateFromByteArray(key);
            var cryptKey = crypt.CreateKey(keyBuffer);

            var dataBuffer = Windows.Security.Cryptography.CryptographicBuffer.CreateFromByteArray(buffer);
            var signBuffer = Windows.Security.Cryptography.Core.CryptographicEngine.Sign(cryptKey, dataBuffer);

            byte[] value;
            Windows.Security.Cryptography.CryptographicBuffer.CopyToByteArray(signBuffer, out value);
            return value;
        }
    }
}
