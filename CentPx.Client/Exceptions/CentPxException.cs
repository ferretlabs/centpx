﻿using System;
using CentPx.Client.Model.Responses;

namespace CentPx.Client.Exceptions
{
    public class CentPxException : Exception
    {
        internal CentPxException(StatusResponse response)
        {
            Response = response;
        }

        internal CentPxException()
        {
            IsCancelled = true;
        }

        public StatusResponse Response { get; set; }
        public bool IsCancelled { get; }
    }
}
