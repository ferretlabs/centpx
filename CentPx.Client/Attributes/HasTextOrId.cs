﻿using System;

namespace CentPx.Client.Attributes
{
    internal class HasTextOrId : Attribute
    {
        public string Text { get; }
        public int Id { get; }

        public HasTextOrId(string text, int id)
        {
            Text = text;
            Id = id;
        }

        public HasTextOrId(int id) 
            : this(string.Empty, id)
        {
        }
    }
}