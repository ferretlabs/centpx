﻿using System;
using CentPx.Client.Model.Types;

namespace CentPx.Client.Attributes
{
    public class ImagePropertiesAttribute : Attribute
    {
        public int Length { get; }
        public string Dimensions { get; }
        public Cropped Cropped { get; }

        public ImagePropertiesAttribute(int length, string dimensions, Cropped cropped)
        {
            Length = length;
            Dimensions = dimensions;
            Cropped = cropped;
        }
    }
}