﻿using System;

namespace CentPx.Client.Attributes
{
    internal class FeatureInfoAttribute : ApiAttribute
    {
        public FeatureInfoAttribute(string apiToken, string name) 
            : base(apiToken, name)
        {
        }
    }

    internal class ApiAttribute : Attribute
    {
        public string ApiToken { get; }
        public string Name { get; }

        public ApiAttribute(string apiToken, string name)
        {
            ApiToken = apiToken;
            Name = name;
        }
    }

    internal class SortByAttribute : ApiAttribute
    {
        public SortByAttribute(string apiToken, string name) 
            : base(apiToken, name)
        {
        }
    }
}