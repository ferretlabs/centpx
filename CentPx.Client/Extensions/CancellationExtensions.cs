﻿using System.Threading;
using CentPx.Client.Exceptions;
using CentPx.Client.Model.Responses;

namespace CentPx.Client.Extensions
{
    internal static class CancellationExtensions
    {
        internal static void ThrowCentPxExceptionIfCancelled(this CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                throw new CentPxException();
            }
        }
    }
}
