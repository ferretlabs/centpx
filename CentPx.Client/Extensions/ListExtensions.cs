﻿using System.Collections.Generic;
using System.Linq;
using CentPx.Client.Model;
using CentPx.Client.Model.Types;

namespace CentPx.Client.Extensions
{
    public static class ListExtensions
    {
        public static string ToCommaString(this List<CategoryTypes> categories)
        {
            return string.Join(",", categories.Select(x => x.GetCategoryName()));
        }

        public static string ToCommaString(this List<ImageSize> imageSizes)
        {
            return string.Join(",", imageSizes.Select(x => x.GetImageSizeId()));
        }

        public static string ToCommaString(this List<LicenseTypes> licenses)
        {
            return string.Join(",", licenses.Select(x => ((int) x).ToString()));
        }
    }
}
