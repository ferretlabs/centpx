namespace CentPx.Client.Extensions
{
    internal static class BooleanExtensions
    {
        internal static string ToBinary(this bool value)
        {
            return value ? "1" : "0";
        }
    }
}