using System;
using System.Collections.Generic;
using System.Linq;
using CentPx.Client.Model.Requests;

namespace CentPx.Client.Extensions
{
    internal static class DictionaryExtensions
    {
        internal static string ToQueryString(this Dictionary<string, string> options)
        {
            return options.Aggregate("", (s, pair) =>
                s + string.Format("{0}{1}={2}", s.Length > 0 ? "&" : "",
                    Uri.EscapeDataString(pair.Key),
                    pair.Value.ToString().UriEncode()));
        }

        internal static void AddPhotoOptions(this Dictionary<string, string> data, FetchPhotoOptions options)
        {
            if (options == null) return;
            data.AddBaseFetchPhotoOptions(options);

            data.AddIfNotNull("feature", options.Feature?.GetFeatureApiToken());
            data.AddIfNotNull("sort_direction", options.SortDirection?.GetDescription());
            data.AddIfNotNull("include_store", options.IncludeMarketplace);
            data.AddIfNotNull("include_states", "1");
        }

        internal static void AddPhotoOptions(this Dictionary<string, string> data, SearchFetchPhotoOptions options)
        {
            if (options == null) return;
            data.AddBaseFetchPhotoOptions(options);

            data.AddIfNotNull("license_type", options.LicenseTypes?.ToCommaString());
            data.AddIfNotNull("geo", options.GeoLocation?.ToString());
        }

        private static void AddBaseFetchPhotoOptions(this Dictionary<string, string> data, BaseFetchPhotoOptions options)
        {
            if (options == null) return;

            data.AddIfNotNull("only", options.Categories?.ToCommaString());
            data.AddIfNotNull("exclude", options.ExcludeCategories?.ToCommaString());
            data.AddIfNotNull("sort", options.SortBy?.GetSortByApiToken());
            data.AddIfNotNull("image_size", options.ImageSizes?.ToCommaString());
            data.AddIfNotNull("tags", options.IncludeTags);
            data.AddIfNotNull("page", options.Page);
            data.AddIfNotNull("rpp", options.ResultsPerPage);
        }

        internal static void AddUpdatedPhotoOptions(this Dictionary<string, string> data, UpdatePhotoOptions options)
        {
            if (options == null) return;

            data.AddBasePhotoOptions(options);
            data.AddIfNotNull("add_tags", options.TagsToAdd);
            data.AddIfNotNull("remove_tags", options.TagsToRemove);
            data.AddIfNotNull("nsfw", options.IsNsfw);
            data.AddIfNotNull("license_type", options.License?.GetLicenseId());
        }

        internal static void AddUploadPhotoOptions(this Dictionary<string, string> data, UploadPhotoOptions options)
        {
            if (options == null) return;

            data.AddBasePhotoOptions(options);
        }

        internal static void AddGalleryOptions(this Dictionary<string, string> data, FetchGalleryOptions options)
        {
            if (options == null) return;

            data.AddIfNotNull("sort", options.SortBy);
            data.AddIfNotNull("cover_size", options.CoverImageSize);
            data.AddIfNotNull("include_cover", options.IncludeCover);
            data.AddIfNotNull("sort_direction", options.SortDirection);
            data.AddIfNotNull("privacy", options.GalleryPrivacy);
            data.AddIfNotNull("kinds", options.GalleryTypes?.Select(x=> x.GetGalleryKindId()).ToList());
            data.AddIfNotNull("page", options.Page);
            data.AddIfNotNull("rpp", options.ResultsPerPage);
        }

        internal static void AddGalleryPhotoOptions(this Dictionary<string, string> data, FetchGalleryPhotoOptions options)
        {
            if (options == null) return;

            data.AddPhotoOptions(options);
            data.AddIfNotNull("include_missing", options.IncludeMissingPhotos);
            data.AddIfNotNull("include_geo", options.IncludeGeoLocation);
            data.AddIfNotNull("include_licensing", options.IncludeLicensing);
        }

        internal static void AddUpdateGalleryOptions(this Dictionary<string, string> data, UpdateGalleryOptions options)
        {
            if (options == null) return;

            data.AddBaseGalleryOptions(options);
        }

        internal static void AddAddGalleryOptions(this Dictionary<string, string> data, AddGalleryOptions options)
        {
            if (options == null) return;

            data.AddBaseGalleryOptions(options);
            data.Add("kind", options.GalleryType.GetGalleryKindId());
        }

        private static void AddBaseGalleryOptions(this Dictionary<string, string> data, BaseGalleryOptions options)
        {
            if (options == null) return;

            data.AddIfNotNull("name", options.Name);
            data.AddIfNotNull("description", options.Description);
            data.AddIfNotNull("subtitle", options.Subtitle);
            data.AddIfNotNull("privacy", options.IsPrivate);
            data.AddIfNotNull("cover_photo_id", options.CoverPhotoId);
            data.AddIfNotNull("custom_path", options.CustomPath);
        }

        private static void AddBasePhotoOptions(this Dictionary<string, string> data, BasePhotoOptions options)
        {
            if (options == null) return;

            data.AddIfNotNull("name", options.Name);
            data.AddIfNotNull("description", options.Description);
            data.AddIfNotNull("category", options.Category?.GetCategoryId());
            data.AddIfNotNull("tags", options.Tags);
            data.AddIfNotNull("shutter_speed", options.ShutterSpeed);
            data.AddIfNotNull("focal_length", options.FocalLength);
            data.AddIfNotNull("aperture", options.Aperture);
            data.AddIfNotNull("iso", options.Iso);
            data.AddIfNotNull("camera", options.Camera);
            data.AddIfNotNull("lens", options.Lens);
            data.AddIfNotNull("latitude", options.Latitude);
            data.AddIfNotNull("longitude", options.Longitude);
            data.AddIfNotNull("privacy", options.IsPrivate?.ToBinary());
        }

        internal static void AddIfNotNull<T>(this Dictionary<string, string> postData, string key, T? item) where T : struct
        {
            if (item.HasValue)
            {
                postData.Add(key, item.Value.ToString());
            }
        }

        internal static void AddIfNotNull<T>(this Dictionary<string, string> postData, string key, T? item, Func<T, string> func) where T : struct
        {
            if (item.HasValue)
            {
                postData.Add(key, func(item.Value));
            }
        }

        internal static void AddIfNotNull(this Dictionary<string, string> postData, string key, bool? item, bool isBinary = true)
        {
            if (item.HasValue)
            {
                postData.Add(key, isBinary ? item.Value.ToBinary() : item.Value.ToString());
            }
        }

        internal static void AddIfNotNull(this Dictionary<string, string> postData, string key, string item)
        {
            if (!string.IsNullOrEmpty(item))
            {
                postData.Add(key, item);
            }
        }

        internal static void AddIfNotNull<T>(this Dictionary<string, string> postData, string key, List<T> item)
        {
            if (item != null && item.Any())
            {
                var data = $"{string.Join(",", item)}";
                postData.Add(key, data);
            }
        }

        internal static T GetValue<T>(this List<KeyValuePair<string, object>> items, string key)
        {
            var item = items.FirstOrDefault(x => x.Key == key);
            var value = (T)item.Value;
            return value;
        }
    }
}