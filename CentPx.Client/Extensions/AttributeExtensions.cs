﻿using System;
using System.Linq;
using System.Reflection;
using CentPx.Client.Attributes;
using CentPx.Client.Model;
using CentPx.Client.Model.Types;

namespace CentPx.Client.Extensions
{
    internal static class AttributeExtensions
    {
        public static string GetDescription(this object en)
        {
            var attribute = en.GetAttribute<Description>();
            if (attribute != null)
            {
                return attribute.Text;
            }

            return en.ToString();
        }

        public static string GetFeatureApiToken(this Feature feature)
        {
            var attribute = feature.GetAttribute<ApiAttribute>();
            return attribute.ApiToken;
        }

        public static string GetFeatureName(this Feature feature)
        {
            var attribute = feature.GetAttribute<ApiAttribute>();
            return attribute.Name;
        }

        public static string GetSortByApiToken(this SortBy feature)
        {
            var attribute = feature.GetAttribute<ApiAttribute>();
            return attribute.ApiToken;
        }

        public static string GetSortByName(this SortBy feature)
        {
            var attribute = feature.GetAttribute<ApiAttribute>();
            return attribute.Name;
        }

        public static string GetCategoryName(this CategoryTypes categoryType)
        {
            var attribute = categoryType.GetAttribute<Description>();
            
            if (!string.IsNullOrEmpty(attribute?.Text))
            {
                return attribute.Text;
            }

            return categoryType.ToString();
        }

        public static string GetCategoryId(this CategoryTypes categoryType)
        {
            var id = (int) categoryType;
            return id.ToString();
        }

        public static string GetLicenseId(this LicenseTypes license)
        {
            var id = (int) license;
            return id.ToString();
        }

        public static string GetReasonId(this ReportReason reason)
        {
            var id = (int)reason;
            return id.ToString();
        }

        public static string GetGalleryKindId(this GalleryTypes galleryType)
        {
            var id = (int)galleryType;
            return id.ToString();
        }

        public static string GetImageSizeId(this ImageSize imageSize)
        {
            return ((int)imageSize).ToString();
        }

        public static int GetImageLength(this ImageSize imageSize)
        {
            var attribute = imageSize.GetAttribute<ImagePropertiesAttribute>();
            return attribute.Length;
        }

        public static Cropped? GetImageSizeCropping(this ImageSize imageSize)
        {
            var attribute = imageSize.GetAttribute<ImagePropertiesAttribute>();
            return attribute?.Cropped;
        }

        public static string GetImageSizeDimensions(this ImageSize imageSize)
        {
            var attribute = imageSize.GetAttribute<ImagePropertiesAttribute>();
            return attribute?.Dimensions;
        }

        private static T GetAttribute<T>(this object en) where T : Attribute
        {
            var type = en.GetType();
            var memInfo = type.GetTypeInfo().DeclaredMembers;

            var attrs = memInfo.FirstOrDefault(x => x.Name == en.ToString());
            var attribute = attrs.GetCustomAttribute<T>();
            return attribute;
        }
    }
}