﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AsyncOAuth;
using CentPx.Client.Exceptions;
using CentPx.Client.Extensions;
using CentPx.Client.Helpers;
using CentPx.Client.Logging;
using CentPx.Client.Model;
using CentPx.Client.Model.Requests;
using CentPx.Client.Model.Responses;
using CentPx.Client.Model.Types;
using Newtonsoft.Json;

namespace CentPx.Client
{
    public interface ICentPxClient
    {
        AccessToken AccessToken { get; }
        Task<string> GetAuthorisationUrl();
        Task<AccessToken> SwapForAccessToken(string code);
        void SetAccessToken(AccessToken accessToken);

        #region User methods
        Task<User> GetSignedInUser(CancellationToken cancellationToken = default(CancellationToken));
        Task<User> GetUserDetails(string username, CancellationToken cancellationToken = default(CancellationToken));
        Task<User> GetUserDetails(int userId, CancellationToken cancellationToken = default(CancellationToken));
        Task<FriendsResponse> GetUserFriends(int userId, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<FriendsResponse> GetUserFollowers(int userId, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<UserSearchResponse> SearchUsers(string searchTerm, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<User> AddUserToFriendsList(int userId, CancellationToken cancellationToken = default(CancellationToken));
        Task<User> RemoveUserFromFriendsList(int userId, CancellationToken cancellationToken = default(CancellationToken));
        #endregion

        #region Photo methods
        Task<PhotosResponse> GetPhotosForUser(int userId, FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetPopularPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetHighestRatedPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetUpcomingPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetEditorsChoicePhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetTodaysFreshPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetYesterdaysFreshPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetThisWeeksFreshPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetFriendsPhotos(int userId, FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> SearchPhotos(string searchTerm, SearchFetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotoResponse> GetPhoto(int photoId, ImageSize? imageSize = null, bool? includeComments = null, int? commentsPage = null, bool? includeTags = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<CommentsResponse> GetPhotoComments(int photoId, int page = 1, bool? nestComments = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<UsersResponse> GetPhotoVoteUsers(int photoId, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken));
        Task<Photo> UpdatePhotoDetails(int photoId, UpdatePhotoOptions options = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<Photo> UploadPhoto(Stream imageStream, string filename, UploadPhotoOptions options = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<Photo> LikePhoto(int photoId, CancellationToken cancellationToken = default(CancellationToken));
        Task<Photo> UnlikePhoto(int photoId, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> AddPhotoTags(int photoId, List<string> tags, CancellationToken cancellationToken = default(CancellationToken));
        Task<Comment> AddCommentToPhoto(int photoId, string commentText, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> ReportPhoto(int photoId, ReportReason reason, string reasonDetails, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> DeletePhoto(int photoId, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> DeletePhotoTags(int photoId, List<string> tags, CancellationToken cancellationToken = default(CancellationToken));
        #endregion

        #region Gallery methods

        Task<GalleriesResponse> GetUserGalleries(int userId, int page = 1, int resultsPerPage = 20, FetchGalleryOptions options = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<Gallery> GetGallery(int userId, int galleryId, ImageSize? imageSize, CancellationToken cancellationToken = default(CancellationToken));
        Task<Gallery> GetGallery(int userId, string galleryName, ImageSize? imageSize, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetGalleryPhotos(int userId, int galleryId, int page = 1, int resultsPerPage = 20, FetchGalleryPhotoOptions options = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<PhotosResponse> GetGalleryPhotos(int userId, string galleryName, int page = 1, int resultsPerPage = 20, FetchGalleryPhotoOptions options = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<string> GetGalleryShareUrl(int userId, int galleryId, CancellationToken cancellationToken = default(CancellationToken));
        Task<string> GetGalleryShareUrl(int userId, string galleryName, CancellationToken cancellationToken = default(CancellationToken));
        Task<Gallery> UpdateGallery(int userId, int galleryId, UpdateGalleryOptions options = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<Gallery> UpdateGallery(int userId, string galleryName, UpdateGalleryOptions options = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<Gallery> AddGallery(int userId, AddGalleryOptions options, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> AddPhotosToGallery(int userId, int galleryId, List<Photo> photos, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> AddPhotosToGallery(int userId, string galleryName, List<Photo> photos, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> RemovePhotosFromGallery(int userId, int galleryId, List<Photo> photos, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> RemovePhotosFromGallery(int userId, string galleryName, List<Photo> photos, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> DeleteGallery(int userId, int galleryId, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> DeleteGallery(int userId, string galleryName, CancellationToken cancellationToken = default(CancellationToken));

        #endregion

        #region Comment methods

        Task<Comment> ReplyToComment(Comment comment, string commentText, CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }

    public class CentPxClient : ICentPxClient
    {
        private const string BaseUrl = "https://api.500px.com/v1/";
        private readonly string _consumerKey;
        private readonly string _consumerSecret;

        private readonly ILogger _logger;

        private HttpClient _httpClient;

        private RequestToken _token;

        public CentPxClient(string consumerKey, string consumerSecret)
            : this(consumerKey, consumerSecret, new NullLogger())
        {

        }

        public CentPxClient(string consumerKey, string consumerSecret, ILogger logger)
        {
            if (string.IsNullOrEmpty(consumerKey))
            {
                throw new ArgumentNullException(nameof(consumerKey), "You must provide a consumer key");
            }
            if (string.IsNullOrEmpty(consumerSecret))
            {
                throw new ArgumentNullException(nameof(consumerSecret), "You must provide a consumer secret");
            }

            _consumerKey = consumerKey;
            _consumerSecret = consumerSecret;
            _logger = logger;

            OAuthUtility.ComputeHash = HashHelpers.ComputeHash;
        }

        public AccessToken AccessToken { get; private set; }

        public async Task<string> GetAuthorisationUrl()
        {
            var authoriser = new OAuthAuthorizer(_consumerKey, _consumerSecret);
            var tokenResponse = await authoriser.GetRequestToken(BaseUrl + "oauth/request_token").ConfigureAwait(false);

            _token = tokenResponse.Token;
            var url = authoriser.BuildAuthorizeUrl(BaseUrl + "oauth/authorize", _token);

            return url;
        }

        public async Task<AccessToken> SwapForAccessToken(string code)
        {
            var authoriser = new OAuthAuthorizer(_consumerKey, _consumerSecret);
            var accessToken = await authoriser.GetAccessToken(BaseUrl + "oauth/access_token", _token, code).ConfigureAwait(false);

            SetAccessToken(accessToken.Token);

            return AccessToken;
        }

        public void SetAccessToken(AccessToken accessToken)
        {
            AccessToken = accessToken;
            if (AccessToken != null)
            {
                _httpClient = OAuthUtility.CreateOAuthClient(_consumerKey, _consumerSecret, accessToken);
            }
        }

        #region User methods
        public async Task<User> GetSignedInUser(CancellationToken cancellationToken = default(CancellationToken))
        {
            CheckIfAuthenticated();

            var result = await GetResponse<UserResponse>(EndPoints.Users, string.Empty, cancellationToken: cancellationToken).ConfigureAwait(false);

            return result.User;
        }

        public async Task<User> GetUserDetails(string username, CancellationToken cancellationToken = default(CancellationToken))
        {
            CheckIfAuthenticated();

            var options = new Dictionary<string, string>
            {
                {"username", username}
            };

            var result = await GetResponse<UserResponse>(EndPoints.Users, "show", options, cancellationToken: cancellationToken).ConfigureAwait(false);

            return result.User;
        }

        public async Task<User> GetUserDetails(int userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            CheckIfAuthenticated();

            var options = new Dictionary<string, string>
            {
                {"id", userId.ToString()}
            };

            var result = await GetResponse<UserResponse>(EndPoints.Users, "show", options, cancellationToken: cancellationToken).ConfigureAwait(false);

            return result.User;
        }

        public async Task<FriendsResponse> GetUserFriends(int userId, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (resultsPerPage > 100)
            {
                resultsPerPage = 100;
            }

            if (page < 1)
            {
                page = 1;
            }

            var options = new Dictionary<string, string>
            {
                {"page", page.ToString()},
                {"rpp", resultsPerPage.ToString()}
            };

            var response = await GetResponse<FriendsResponse>(EndPoints.Users, $"{userId}/friends", options, cancellationToken).ConfigureAwait(false);

            return response;
        }

        public async Task<FriendsResponse> GetUserFollowers(int userId, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (resultsPerPage > 100)
            {
                resultsPerPage = 100;
            }

            if (page < 1)
            {
                page = 1;
            }

            var options = new Dictionary<string, string>
            {
                {"page", page.ToString()},
                {"rpp", resultsPerPage.ToString()}
            };

            var response = await GetResponse<FriendsResponse>(EndPoints.Users, $"{userId}/followers", options, cancellationToken).ConfigureAwait(false);

            return response;
        }

        public async Task<UserSearchResponse> SearchUsers(string searchTerm, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(searchTerm))
            {
                throw new ArgumentNullException(nameof(searchTerm), "Please provide a search term");
            }

            if (resultsPerPage > 100)
            {
                resultsPerPage = 100;
            }

            if (page < 1)
            {
                page = 1;
            }

            var options = new Dictionary<string, string>
            {
                {"term", searchTerm },
                {"page", page.ToString()},
                {"rpp", resultsPerPage.ToString()}
            };

            var response = await GetResponse<UserSearchResponse>(EndPoints.Users, "search", options, cancellationToken).ConfigureAwait(false);
            return response;
        }

        public Task<User> AddUserToFriendsList(int userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return PostResponse<User>(EndPoints.Users, $"{userId}/friends", cancellationToken: cancellationToken);
        }

        public Task<User> RemoveUserFromFriendsList(int userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return DeleteResponse<User>(EndPoints.Users, $"{userId}/friends", cancellationToken: cancellationToken);
        }
        #endregion

        #region Photo methods
        public Task<PhotosResponse> GetPhotosForUser(int userId, FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            var photoOptions = new Dictionary<string, string>
            {
                {"user_id", userId.ToString() }
            };

            if (options == null)
            {
                options = new FetchPhotoOptions();
            }

            options.Feature = Feature.User;

            return GetPhotos(options, photoOptions, page, resultsPerPage, cancellationToken);
        }

        public Task<PhotosResponse> GetFriendsPhotos(int userId, FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            var photoOptions = new Dictionary<string, string>
            {
                {"user_id", userId.ToString() }
            };

            if (options == null)
            {
                options = new FetchPhotoOptions();
            }

            options.Feature = Feature.FollowedUsers;

            return GetPhotos(options, photoOptions, page, resultsPerPage, cancellationToken);
        }

        public Task<PhotosResponse> SearchPhotos(string searchTerm, SearchFetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(searchTerm))
            {
                throw new ArgumentNullException(nameof(searchTerm), "Please provide a search term");
            }

            if (options == null)
            {
                options = new SearchFetchPhotoOptions();
            }

            var photoOptions = new Dictionary<string, string>
            {
                {"term", searchTerm}
            };

            if (resultsPerPage > 100)
            {
                resultsPerPage = 100;
            }

            if (page < 1)
            {
                page = 1;
            }

            options.Page = page;
            options.ResultsPerPage = resultsPerPage;
            photoOptions.AddPhotoOptions(options);

            return GetResponse<PhotosResponse>(EndPoints.Photos, "search", photoOptions, cancellationToken);
        }

        public Task<PhotoResponse> GetPhoto(int photoId, ImageSize? imageSize = null, bool? includeComments = null, int? commentsPage = null, bool? includeTags = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var photoOptions = new Dictionary<string, string>();

            photoOptions.AddIfNotNull("image_size", imageSize?.GetImageSizeId());
            photoOptions.AddIfNotNull("comments", includeComments);
            photoOptions.AddIfNotNull("comments_page", commentsPage);
            photoOptions.AddIfNotNull("tags", includeTags);

            return GetResponse<PhotoResponse>(EndPoints.Photos, photoId.ToString(), photoOptions, cancellationToken);
        }

        public Task<CommentsResponse> GetPhotoComments(int photoId, int page = 1, bool? nestComments = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (page < 1)
            {
                page = 1;
            }

            var photoOptions = new Dictionary<string, string>
            {
                {"page", page.ToString()}
            };
            photoOptions.AddIfNotNull("nested", nestComments);

            return GetResponse<CommentsResponse>(EndPoints.Photos, $"{photoId}/comments", photoOptions, cancellationToken);
        }

        public Task<UsersResponse> GetPhotoVoteUsers(int photoId, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (resultsPerPage > 100)
            {
                resultsPerPage = 100;
            }

            if (page < 1)
            {
                page = 1;
            }

            var options = new Dictionary<string, string>
            {
                {"page", page.ToString()},
                {"rpp", resultsPerPage.ToString()}
            };

            return GetResponse<UsersResponse>(EndPoints.Photos, $"{photoId}/votes", options, cancellationToken);
        }

        public Task<Photo> UpdatePhotoDetails(int photoId, UpdatePhotoOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var photoOptions = new Dictionary<string, string>();
            photoOptions.AddUpdatedPhotoOptions(options);

            return PutResponse<Photo>(EndPoints.Photos, photoId.ToString(), photoOptions, cancellationToken);
        }

        public async Task<Photo> UploadPhoto(Stream imageStream, string filename, UploadPhotoOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(filename))
            {
                throw new ArgumentNullException(nameof(filename), "Please provide a filename");
            }

            var photoOptions = new Dictionary<string, string>();
            photoOptions.AddUploadPhotoOptions(options);

            var response = await PostMultiPartResponse<PhotoResponse>(EndPoints.Photos, "upload", imageStream, filename + ".jpg", photoOptions, cancellationToken).ConfigureAwait(false);
            return response.Photo;
        }

        public async Task<Photo> LikePhoto(int photoId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var options = new Dictionary<string, string>
            {
                {"vote", "1" }
            };

            var response = await PostResponse<Photo>(EndPoints.Photos, $"{photoId}/vote", options, cancellationToken).ConfigureAwait(false);
            return response;
        }

        public async Task<Photo> UnlikePhoto(int photoId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var response = await DeleteResponse<Photo>(EndPoints.Photos, $"{photoId}/vote", new Dictionary<string, string>(), cancellationToken).ConfigureAwait(false);
            return response;
        }

        public async Task<bool> AddPhotoTags(int photoId, List<string> tags, CancellationToken cancellationToken = default(CancellationToken))
        {
            var options = new Dictionary<string, string>();
            options.AddIfNotNull("tags", tags);

            var response = await PostResponse<StatusResponse>(EndPoints.Photos, $"{photoId}/tags", options, cancellationToken).ConfigureAwait(false);
            return response.Status == HttpStatusCode.OK;
        }

        public async Task<Comment> AddCommentToPhoto(int photoId, string commentText, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(commentText))
            {
                throw new ArgumentNullException(nameof(commentText), "Comment text must have something in it");
            }

            var option = new Dictionary<string, string>
            {
                {"body", commentText}
            };

            var response = await PostResponse<AddCommentResponse>(EndPoints.Photos, $"{photoId}/comments", option, cancellationToken).ConfigureAwait(false);
            return response.Comment;
        }

        public async Task<bool> ReportPhoto(int photoId, ReportReason reason, string reasonDetails, CancellationToken cancellationToken = default(CancellationToken))
        {
            var options = new Dictionary<string, string>
            {
                {"reason", reason.GetReasonId()}
            };
            options.AddIfNotNull("reason_details", reasonDetails);

            var response = await PostResponse<BooleanResponse>(EndPoints.Photos, $"{photoId}/report", options, cancellationToken).ConfigureAwait(false);
            return response.Success;
        }

        public async Task<bool> DeletePhoto(int photoId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var response = await DeleteResponse<StatusResponse>(EndPoints.Photos, photoId.ToString(), new Dictionary<string, string>(), cancellationToken).ConfigureAwait(false);
            return response.Status == HttpStatusCode.OK;
        }

        public async Task<bool> DeletePhotoTags(int photoId, List<string> tags, CancellationToken cancellationToken = default(CancellationToken))
        {
            var options = new Dictionary<string, string>();
            options.AddIfNotNull("tags", tags);

            var response = await DeleteResponse<StatusResponse>(EndPoints.Photos, $"{photoId}/tags", options, cancellationToken).ConfigureAwait(false);
            return response.Status == HttpStatusCode.OK;
        }

        public Task<PhotosResponse> GetPopularPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (options == null)
            {
                options = new FetchPhotoOptions();
            }

            options.Feature = Feature.Popular;

            return GetPhotos(options, new Dictionary<string, string>(), page, resultsPerPage, cancellationToken);
        }

        public Task<PhotosResponse> GetHighestRatedPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (options == null)
            {
                options = new FetchPhotoOptions();
            }

            options.Feature = Feature.HighestRated;

            return GetPhotos(options, new Dictionary<string, string>(), page, resultsPerPage, cancellationToken);
        }

        public Task<PhotosResponse> GetUpcomingPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (options == null)
            {
                options = new FetchPhotoOptions();
            }

            options.Feature = Feature.Upcoming;

            return GetPhotos(options, new Dictionary<string, string>(), page, resultsPerPage, cancellationToken);
        }

        public Task<PhotosResponse> GetEditorsChoicePhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (options == null)
            {
                options = new FetchPhotoOptions();
            }

            options.Feature = Feature.Editors;

            return GetPhotos(options, new Dictionary<string, string>(), page, resultsPerPage, cancellationToken);
        }

        public Task<PhotosResponse> GetTodaysFreshPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (options == null)
            {
                options = new FetchPhotoOptions();
            }

            options.Feature = Feature.FreshToday;

            return GetPhotos(options, new Dictionary<string, string>(), page, resultsPerPage, cancellationToken);
        }

        public Task<PhotosResponse> GetYesterdaysFreshPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (options == null)
            {
                options = new FetchPhotoOptions();
            }

            options.Feature = Feature.FreshYesterday;

            return GetPhotos(options, new Dictionary<string, string>(), page, resultsPerPage, cancellationToken);
        }

        public Task<PhotosResponse> GetThisWeeksFreshPhotos(FetchPhotoOptions options = null, int page = 1, int resultsPerPage = 20, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (options == null)
            {
                options = new FetchPhotoOptions();
            }

            options.Feature = Feature.FreshThisWeek;

            return GetPhotos(options, new Dictionary<string, string>(), page, resultsPerPage, cancellationToken);
        }

        private Task<PhotosResponse> GetPhotos(FetchPhotoOptions options, Dictionary<string, string> photoOptions, int page, int resultsPerPage, CancellationToken cancellationToken)
        {
            if (resultsPerPage > 100)
            {
                resultsPerPage = 100;
            }

            if (page < 1)
            {
                page = 1;
            }

            options.Page = page;
            options.ResultsPerPage = resultsPerPage;
            photoOptions.AddPhotoOptions(options);

            return GetResponse<PhotosResponse>(EndPoints.Photos, string.Empty, photoOptions, cancellationToken);
        }

        #endregion

        #region Gallery methods
        public Task<GalleriesResponse> GetUserGalleries(int userId, int page = 1, int resultsPerPage = 20, FetchGalleryOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (resultsPerPage > 100)
            {
                resultsPerPage = 100;
            }

            if (page < 1)
            {
                page = 1;
            }

            if (options == null)
            {
                options = new FetchGalleryOptions();
            }

            options.Page = page;
            options.ResultsPerPage = resultsPerPage;

            var galleryOptions = new Dictionary<string, string>();
            galleryOptions.AddGalleryOptions(options);

            return GetResponse<GalleriesResponse>(EndPoints.Users, $"{userId}/galleries", galleryOptions, cancellationToken);
        }

        public Task<Gallery> GetGallery(int userId, int galleryId, ImageSize? imageSize, CancellationToken cancellationToken = default(CancellationToken))
        {
            return GetGallery(userId, galleryId.ToString(), imageSize, cancellationToken);
        }

        public Task<Gallery> GetGallery(int userId, string galleryName, ImageSize? imageSize, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(galleryName))
            {
                throw new ArgumentNullException(nameof(galleryName), "You must provide a gallery name");
            }

            var options = new Dictionary<string, string>();
            options.AddIfNotNull("image_size", imageSize);

            return GetResponse<Gallery>(EndPoints.Users, $"{userId}/galleries/{galleryName}", options, cancellationToken);
        }

        public Task<PhotosResponse> GetGalleryPhotos(int userId, int galleryId, int page = 1, int resultsPerPage = 20, FetchGalleryPhotoOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            return GetGalleryPhotos(userId, galleryId.ToString(), page, resultsPerPage, options, cancellationToken);
        }

        public Task<PhotosResponse> GetGalleryPhotos(int userId, string galleryName, int page = 1, int resultsPerPage = 20, FetchGalleryPhotoOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(galleryName))
            {
                throw new ArgumentNullException(nameof(galleryName), "You must provide a gallery name");
            }

            if (resultsPerPage > 100)
            {
                resultsPerPage = 100;
            }

            if (page < 1)
            {
                page = 1;
            }

            if (options == null)
            {
                options = new FetchGalleryPhotoOptions();
            }

            options.Page = page;
            options.ResultsPerPage = resultsPerPage;

            var photoOptions = new Dictionary<string, string>();
            photoOptions.AddGalleryPhotoOptions(options);

            return GetResponse<PhotosResponse>(EndPoints.Users, $"{userId}/galleries/{galleryName}/items", photoOptions, cancellationToken);
        }

        public Task<string> GetGalleryShareUrl(int userId, int galleryId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return GetGalleryShareUrl(userId, galleryId.ToString(), cancellationToken);
        }

        public async Task<string> GetGalleryShareUrl(int userId, string galleryName, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(galleryName))
            {
                throw new ArgumentNullException(nameof(galleryName), "You must provide a gallery name");
            }

            var response = await GetResponse<ShareUrlResponse>(EndPoints.Users, $"{userId}/galleries/{galleryName}/share_url", new Dictionary<string, string>(), cancellationToken).ConfigureAwait(false);
            return response.ShareUrl;
        }

        public Task<Gallery> UpdateGallery(int userId, int galleryId, UpdateGalleryOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            return UpdateGallery(userId, galleryId.ToString(), options, cancellationToken);
        }

        public Task<Gallery> UpdateGallery(int userId, string galleryName, UpdateGalleryOptions options = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(galleryName))
            {
                throw new ArgumentNullException(nameof(galleryName), "You must provide a gallery name");
            }

            var galleryOptions = new Dictionary<string, string>();
            galleryOptions.AddUpdateGalleryOptions(options);

            return PutResponse<Gallery>(EndPoints.Users, $"{userId}/galleries/{galleryName}", galleryOptions, cancellationToken);
        }

        public async Task<Gallery> AddGallery(int userId, AddGalleryOptions options, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(options.Name))
            {
                throw new ArgumentNullException(nameof(options.Name), "You must provide a name");
            }

            var galleryOptions = new Dictionary<string, string>();
            galleryOptions.AddAddGalleryOptions(options);

            var response = await PostResponse<GalleryResponse>(EndPoints.Users, $"{userId}/galleries", galleryOptions, cancellationToken).ConfigureAwait(false);
            return response.Gallery;
        }

        public Task<bool> AddPhotosToGallery(int userId, int galleryId, List<Photo> photos, CancellationToken cancellationToken = default(CancellationToken))
        {
            return AddPhotosToGallery(userId, galleryId.ToString(), photos, cancellationToken);
        }

        public Task<bool> AddPhotosToGallery(int userId, string galleryName, List<Photo> photos, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(galleryName))
            {
                throw new ArgumentNullException(nameof(galleryName), "You must provide a gallery name");
            }

            return AddRemovePhotosGallery(userId, galleryName, photos, null, cancellationToken);
        }

        private async Task<bool> AddRemovePhotosGallery(int userId, string galleryName, List<Photo> photosToAdd, List<Photo> photosToRemove, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(galleryName))
            {
                throw new ArgumentNullException(nameof(galleryName), "You must provide a gallery name");
            }

            if ((photosToAdd == null || !photosToAdd.Any())
                && (photosToRemove == null || !photosToRemove.Any()))
            {
                throw new ArgumentNullException(nameof(photosToAdd), "You need to provide some photos");
            }

            var addIds = photosToAdd?.Select(x => x.Id).ToList();
            var removeIds = photosToRemove?.Select(x => x.Id).ToList();
            var add = new AddRemoveGalleryPhotos
            {
                Add = new Add
                {
                    Photos = addIds
                },
                Remove = new Remove
                {
                    Photos = removeIds
                }
            };

            var response = await PutJsonResponse<object>(EndPoints.Users, $"{userId}/galleries/{galleryName}/items", add, cancellationToken).ConfigureAwait(false);
            return response != null;
        }

        public Task<bool> RemovePhotosFromGallery(int userId, int galleryId, List<Photo> photos, CancellationToken cancellationToken = default(CancellationToken))
        {
            return RemovePhotosFromGallery(userId, galleryId.ToString(), photos, cancellationToken);
        }

        public Task<bool> RemovePhotosFromGallery(int userId, string galleryName, List<Photo> photos, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(galleryName))
            {
                throw new ArgumentNullException(nameof(galleryName), "You must provide a gallery name");
            }

            return AddRemovePhotosGallery(userId, galleryName, null, photos, cancellationToken);
        }

        public Task<bool> DeleteGallery(int userId, int galleryId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return DeleteGallery(userId, galleryId.ToString(), cancellationToken);
        }

        public async Task<bool> DeleteGallery(int userId, string galleryName, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(galleryName))
            {
                throw new ArgumentNullException(nameof(galleryName), "You must provide a gallery name");
            }

            var response = await DeleteResponse<StatusResponse>(EndPoints.Users, $"{userId}/galleries/{galleryName}", new Dictionary<string, string>(), cancellationToken).ConfigureAwait(false);
            return response.Status == HttpStatusCode.OK;
        }
        
        #endregion

        #region Comment methods

        public async Task<Comment> ReplyToComment(Comment comment, string commentText, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrEmpty(commentText))
            {
                throw new ArgumentNullException(nameof(commentText), "Your comment must have some text in it");
            }

            var id = comment.ParentId ?? comment.Id;
            
            var options = new Dictionary<string, string>
            {
                {"body", commentText }
            };

            var response = await PostResponse<CommentResponse>(EndPoints.Comments, $"{id}/comments", options, cancellationToken).ConfigureAwait(false);
            return response.Comment;
        }

        #endregion

        #region Private methods
        private void CheckIfAuthenticated()
        {
            if (AccessToken == null)
            {
                throw new UnauthorizedAccessException("You have to sign in first before you can make this call");
            }
        }

        private async Task<TResponseType> PostMultiPartResponse<TResponseType>(string endPoint, string method, Stream data, string filename, Dictionary<string, string> options = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
        {
            _logger.Debug(callingMethod);
            var url = string.Format("{0}{1}/{2}", BaseUrl, endPoint, method);

            if (options != null && options.Any())
            {
                var queryString = options.ToQueryString();
                url = $"{url}?{queryString}";
            }

            var requestTime = DateTime.Now;

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                await data.CopyToAsync(stream).ConfigureAwait(false);
                bytes = stream.ToArray();
            }

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            HttpResponseMessage response;
            using (var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
            {
                var imageContent = new ByteArrayContent(bytes);
                imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");
                content.Add(imageContent, "file", filename);

                response = await _httpClient.PostAsync(url, content, cancellationToken).ConfigureAwait(false);
            }

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            var duration = DateTime.Now - requestTime;

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "POST", url);

            return await HandleResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> PostResponse<TResponseType>(string endPoint, string method, Dictionary<string, string> postData = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
        {
            _logger.Debug(callingMethod);
            var url = string.Format("{0}{1}/{2}", BaseUrl, endPoint, method);
            var requestTime = DateTime.Now;

            var parameters = postData != null ? postData.ToQueryString() : string.Empty;

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            var response = await _httpClient.PostAsync(url, new StringContent(parameters), cancellationToken).ConfigureAwait(false);

            var duration = DateTime.Now - requestTime;

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "POST", url);

            return await HandleResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> PutResponse<TResponseType>(string endPoint, string method, Dictionary<string, string> options = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
        {
            _logger.Debug(callingMethod);
            var url = string.Format("{0}{1}/{2}", BaseUrl, endPoint, method);
            if (options != null && options.Any())
            {
                var queryString = options.ToQueryString();
                url = $"{url}?{queryString}";
            }
            var requestTime = DateTime.Now;

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            var response = await _httpClient.PutAsync(url, new StringContent(string.Empty), cancellationToken).ConfigureAwait(false);

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            var duration = DateTime.Now - requestTime;
            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "PUT", url);

            return await HandleResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> PutJsonResponse<TResponseType>(string endPoint, string method, object postData, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
        {
            _logger.Debug(callingMethod);
            var url = string.Format("{0}{1}/{2}", BaseUrl, endPoint, method);
            var requestTime = DateTime.Now;

            var json = JsonConvert.SerializeObject(postData);

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            var response = await _httpClient.PutAsync(url, new StringContent(json, Encoding.UTF8, "application/json"), cancellationToken).ConfigureAwait(false);

            var duration = DateTime.Now - requestTime;
            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "PUT", url);

            return await HandleResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> GetResponse<TResponseType>(string endPoint, string method, Dictionary<string, string> options = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
        {
            _logger.Debug(callingMethod);
            var url = string.Format("{0}{1}/{2}", BaseUrl, endPoint, method);

            if (options != null && options.Any())
            {
                var queryString = options.ToQueryString();
                url = $"{url}?{queryString}";
            }

            _logger.Debug("GET: {0}", url);
            var requestTime = DateTime.Now;

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            var response = await _httpClient.GetAsync(url, cancellationToken).ConfigureAwait(false);
            var duration = DateTime.Now - requestTime;

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "GET", url);

            return await HandleResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private async Task<TResponseType> DeleteResponse<TResponseType>(string endPoint, string method, Dictionary<string, string> options = null, CancellationToken cancellationToken = default(CancellationToken), [CallerMemberName] string callingMethod = "")
        {
            _logger.Debug(callingMethod);
            var url = string.Format("{0}{1}/{2}", BaseUrl, endPoint, method);

            if (options != null && options.Any())
            {
                var queryString = options.ToQueryString();
                url = $"{url}?{queryString}";
            }

            _logger.Debug("DELETE: {0}", url);
            var requestTime = DateTime.Now;

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            var response = await _httpClient.DeleteAsync(url, cancellationToken).ConfigureAwait(false);
            var duration = DateTime.Now - requestTime;

            cancellationToken.ThrowCentPxExceptionIfCancelled();

            _logger.Debug("Received {0} status code after {1} ms from {2}: {3}", response.StatusCode, duration.TotalMilliseconds, "DELETE", url);

            return await HandleResponse<TResponseType>(response).ConfigureAwait(false);
        }

        private static async Task<TResponseType> HandleResponse<TResponseType>(HttpResponseMessage response)
        {
            var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            if (!response.IsSuccessStatusCode)
            {
                var error = JsonConvert.DeserializeObject<StatusResponse>(responseString);
                throw new CentPxException(error);
            }

            var item = await responseString.DeserialiseAsync<TResponseType>().ConfigureAwait(false);
            return item;
        }
        #endregion
    }
}
