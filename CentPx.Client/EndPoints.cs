﻿namespace CentPx.Client
{
    internal static class EndPoints
    {
        internal const string Users = "users";
        internal const string Photos = "photos";
        internal const string Comments = "comments";
        internal const string Oauth = "oauth";
    }

    internal static class Methods
    {
        
    }
}
