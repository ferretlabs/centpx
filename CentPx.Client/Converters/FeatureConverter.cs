﻿using System;
using System.Collections.Generic;
using System.Linq;
using CentPx.Client.Extensions;
using CentPx.Client.Model.Types;
using Newtonsoft.Json;

namespace CentPx.Client.Converters
{
    internal class FeatureConverter : JsonConverter
    {
        private static Dictionary<Feature, string> _features;
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var feature = ((Feature) value).GetFeatureApiToken();
            writer.WriteValue(feature);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (_features == null)
            {
                var types = Enum.GetValues(typeof(Feature)).Cast<Feature>();
                _features = types.Select(x => new KeyValuePair<Feature, string>(x, x.GetFeatureApiToken().ToLower())).ToDictionary(x => x.Key, x => x.Value);
            }

            if (reader.ValueType == typeof(string))
            {
                var result = _features.FirstOrDefault(x => x.Value == reader.Value.ToString().ToLower());

                return result.Key;
            }

            var num = int.Parse(reader.Value.ToString());

            return (Feature)Enum.ToObject(typeof(Feature), num);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }
    }
}
