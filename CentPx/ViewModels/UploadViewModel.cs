﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CentPx.Client.Extensions;
using CentPx.Client.Model;
using CentPx.Client.Model.Types;
using CentPx.Core.Helpers;
using CentPx.Messages;
using CentPx.Model;
using CentPx.Services;
using CentPx.ViewModels.Item;
using CentPx.Views;
using Cimbalino.Toolkit.Extensions;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using JetBrains.Annotations;

namespace CentPx.ViewModels
{
    public class UploadViewModel : BasePageViewModel
    {
        private const string MultipleTitles = "There are multiple titles";
        private const string MultipleKeywords = "There are differing keywords";
        private const string MultipleDescriptions = "There are differing descriptions";
        private const string MultipleLicenses = "There are multiple licenses";
        private const string MultipleCategories = "There are multiple categories";

        private readonly IFilePickerService _filePickerService;

        private bool _ignoreChanges;

        private CancellationTokenSource _cancellationTokenSource;

        public UploadViewModel(IFilePickerService filePickerService, IServices services) 
            : base(services)
        {
            _filePickerService = filePickerService;
            SelectedFiles.CollectionChanged += (sender, args) =>
            {
                RaisePropertyChanged(() => EditingLabel);
                RaisePropertyChanged(() => CanEdit);

                UpdateEditableProperties();
            };

            Files.CollectionChanged += (sender, args) =>
            {
                RaisePropertyChanged(() => CanUpload);
            };

            _ignoreChanges = true;

            var licenses = EnumHelper.GetValues<LicenseTypes>();
            var categories = EnumHelper.GetValues<CategoryTypes>();

            LicenseTypes = licenses.Select(x => new KeyValuePair<LicenseTypes, string>(x, x.GetDescription())).ToList();
            CategoryTypes = categories.Select(x => new KeyValuePair<CategoryTypes, string>(x, x.GetCategoryName())).OrderBy(x => x.Value).ToList();

            Reset();

            _ignoreChanges = false;
        }

        public ObservableCollection<FileViewModel> Files { get; set; } = new ObservableCollection<FileViewModel>();

        public ObservableCollection<FileViewModel> SelectedFiles { get; set; } = new ObservableCollection<FileViewModel>();

        public bool ShowFullPageBrowse { get; set; } = true;

        public string EditingLabel => SelectedFiles.Count == 1 ? "Editing 1 photo" : $"Editing {SelectedFiles.Count} photos";
        public bool CanEdit => SelectedFiles.Any();

        public List<KeyValuePair<LicenseTypes, string>> LicenseTypes { get; }
        public List<KeyValuePair<CategoryTypes, string>> CategoryTypes { get; }

        public string Title { get; set; }
        public string Description { get; set; }
        public KeyValuePair<CategoryTypes, string> Category { get; set; }
        public KeyValuePair<LicenseTypes, string> License { get; set; }
        public string Keywords { get; set; }
        public bool? IsAdult { get; set; } = false;
        public bool? IsPrivate { get; set; } = false;
        
        public string MultiCategories { get; } = MultipleCategories;
        public string MultiLicenses { get; } = MultipleLicenses;
        public bool ShowMultiCategories { get; set; }
        public bool ShowMultiLicenses { get; set; }

        public bool IsUploading { get; set; }
        public bool CanUpload => Files.Any() && !IsUploading;
        
        public RelayCommand<List<File>> DroppedFilesCommand => new RelayCommand<List<File>>(AddFiles);

        public RelayCommand BrowseCommand
        {
            get
            {
                return new RelayCommand(async () =>
                {
                    var result = await _filePickerService.PickMultipleFilesAsync(new FilePickerServiceOptions
                    {
                        FileTypeFilters = new List<string> { ".jpg", ".jpeg", ".png" },
                        SuggestedStartLocation = FilePickerServiceLocationId.PicturesLibrary,
                        ViewMode = FilePickerServiceViewMode.Thumbnail
                    });

                    var files = result.Select(x => new ImageFile(x));
                    AddFiles(files);
                });
            }
        }

        public RelayCommand PublishPhotosCommand
        {
            get
            {
                return new RelayCommand(async () =>
                {
                    IsUploading = true;

                    _cancellationTokenSource = new CancellationTokenSource();
                    
                    var tasks = Files.Where(x => !x.IsDummyType).Select(x => x.UploadPhoto(_cancellationTokenSource.Token));

                    await Task.WhenAll(tasks);

                    IsUploading = false;

                    if (Navigation.CanGoBack)
                    {
                        Navigation.GoBack();
                    }
                    else
                    {
                        Navigation.Navigate<HomeView>();
                    }

                    ShowFullPageBrowse = true;
                });
            }
        }

        public RelayCommand CancelCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    _cancellationTokenSource?.Cancel();
                });
            }
        }

        public override Task OnNavigatedFromAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            SelectedFiles.Clear();
            Files.Clear();

            return base.OnNavigatedFromAsync(eventArgs);
        }

        private void AddFiles(IEnumerable<File> files)
        {
            if (!files.Any())
            {
                return;
            }

            var imageFiles = files.Cast<ImageFile>();

            CheckAndAddDummyItem();

            var images = imageFiles.Select(x => new FileViewModel(x, Services));
            Files.AddRange(images);

            ShowFullPageBrowse = !Files.Any();
        }
        
        [UsedImplicitly]
        private void OnTitleChanged()
        {
            SetProperty(file => file.Title = Title);
        }

        [UsedImplicitly]
        private void OnDescriptionChanged()
        {
            SetProperty(file => file.Description = Description);
        }

        [UsedImplicitly]
        private void OnCategoryChanged()
        {
            SetProperty(file => file.Category = Category.Key);

            CheckForMultipleCategories();
        }

        [UsedImplicitly]
        private void OnLicenseChanged()
        {
            SetProperty(file => file.License = License.Key);

            CheckForMultipleLicenses();
        }

        [UsedImplicitly]
        private void OnKeywordsChanged()
        {
            SetProperty(file => file.Keywords = Keywords);
        }

        [UsedImplicitly]
        private void OnIsAdultChanged()
        {
            SetProperty(file => file.IsAdult = IsAdult ?? false);
        }

        [UsedImplicitly]
        private void OnIsPrivateChanged()
        {
            SetProperty(file => file.IsPrivate = IsPrivate ?? false);
        }

        private void SetProperty(Action<FileViewModel> func)
        {
            if (_ignoreChanges) return;

            foreach (var file in SelectedFiles)
            {
                func(file);
            }
        }

        private void UpdateEditableProperties()
        {
            if (!SelectedFiles.Any())
            {
                Reset();
                return;
            }

            _ignoreChanges = true;

            var titles = SelectedFiles.Select(x => x.Title).Distinct().ToList();
            Title = titles.Count == 1 ? titles[0] : MultipleTitles;

            var keywords = SelectedFiles.Select(x => x.Keywords).Distinct().ToList();
            Keywords = keywords.Count == 1 ? keywords[0] : MultipleKeywords;

            if (SelectedFiles.Any(x => x.IsAdult) && SelectedFiles.Any(x => !x.IsAdult))
            {
                IsAdult = null;
            }
            else
            {
                IsAdult = SelectedFiles[0].IsAdult;
            }

            if (SelectedFiles.Any(x => x.IsPrivate) && SelectedFiles.Any(x => !x.IsPrivate))
            {
                IsPrivate = null;
            }
            else
            {
                IsPrivate = SelectedFiles[0].IsPrivate;
            }

            var descriptions = SelectedFiles.Select(x => x.Description?.Trim()).Distinct().ToList();
            Description = descriptions.Count == 1 ? descriptions[0] : MultipleDescriptions;

            CheckForMultipleCategories();

            CheckForMultipleLicenses();

            _ignoreChanges = false;
        }

        private void CheckForMultipleLicenses()
        {
            var lics = SelectedFiles.Select(x => x.License).Distinct().ToList();
            ShowMultiLicenses = lics.Count > 1;
        }

        private void CheckForMultipleCategories()
        {
            var cats = SelectedFiles.Select(x => x.Category).Distinct().ToList();
            ShowMultiCategories = cats.Count > 1;
        }

        private void CheckAndAddDummyItem()
        {
            if (!Files.Any())
            {
                Files.Add(new FileViewModel(null, null) { IsDummyType = true });
            }
        }

        private void Reset()
        {
            License = LicenseTypes.First();
            Category = CategoryTypes.First(x => x.Key == Client.Model.CategoryTypes.Uncategorized);

            IsAdult = false;
            IsPrivate = false;
            Title = null;
            Description = null;
            Keywords = null;
        }

        protected override void WireMessages()
        {
            Messenger.Default.Register<RemoveFileMessage>(this, m =>
            {
                SelectedFiles.Remove(m.File);
                Files.Remove(m.File);
            });
        }
    }
}
