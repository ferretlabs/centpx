﻿using System.Linq;
using System.Threading.Tasks;
using CentPx.Client.Exceptions;
using CentPx.Collections;
using CentPx.Core.Extensions;
using CentPx.Services;
using CentPx.ViewModels.Item;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Command;

namespace CentPx.ViewModels
{
    public class UserProfileViewModel : BasePageViewModel
    {
        public UserProfileViewModel(IServices services) : base(services)
        {
        }

        public UserViewModel User { get; set; }

        public IncrementalLoadingCollectionWithId<UserProfilePhotoCollection, PhotoViewModel> Photos { get; set; }

        public override async Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            var user = eventArgs.Parameter as UserViewModel;
            if (user == null)
            {
                await Dispatcher.InvokeOnUiThreadAsync(() =>
                {
                    Navigation.GoBack();
                });
            }
            else
            {
                User = user;
                ReloadProfile().DontAwait("Let the intial page load and update it when the profile is reloaded.");
                LoadPhotos(false).DontAwait("Just go off and load photos, don't stop the page from loading");
            }
        }

        public override Task OnNavigatedFromAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            if (eventArgs.NavigationMode == NavigationServiceNavigationMode.Back)
            {
                Photos = null;
                User = null;
            }

            return base.OnNavigatedFromAsync(eventArgs);
        }

        public RelayCommand RefreshPhotosCommand
        {
            get
            {
                return new RelayCommand(async () =>
                {
                    await LoadPhotos(true);
                });
            }
        }

        private async Task ReloadProfile()
        {
            try
            {
                var user = await CentPx.GetUserDetails(User.Username);
                if (user != null)
                {
                    User.ReloadUser(user);
                }
            }
            catch (CentPxException)
            {

            }
            finally
            {

            }
        }

        private Task LoadPhotos(bool isRefresh)
        {
            if (!isRefresh && Photos != null && Photos.Any())
            {
                return Task.FromResult(0);
            }

            try
            {
                SetProgressBar("Loading user photos...");

                if (Photos == null || isRefresh)
                {
                    Photos = new IncrementalLoadingCollectionWithId<UserProfilePhotoCollection, PhotoViewModel>(Services);
                }

                Photos.SetId(User.User.Id);
            }
            catch (CentPxException)
            {

            }
            finally
            {
                SetProgressBar();
            }

            return Task.FromResult(0);
        }
    }
}