﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentPx.Client.Model;
using CentPx.Client.Model.Requests;
using CentPx.Collections;
using CentPx.Core.Helpers;
using CentPx.Services;
using CentPx.ViewModels.Item;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Command;
using JetBrains.Annotations;

namespace CentPx.ViewModels
{
    public class SearchViewModel : BasePageViewModel
    {
        public SearchViewModel(IServices services) 
            : base(services)
        {
            SearchTypes = EnumHelper.GetValues<SearchType>();
            Categories = EnumHelper.GetValues<CategoryTypes>();
        }

        public string SearchTerm { get; set; }

        public List<SearchType> SearchTypes { get; set; }
        public SearchType SearchType { get; set; }

        public List<CategoryTypes> Categories { get; set; }
        public CategoryTypes Category { get; set; }

        public ServiceBasedIncrementalLoadingCollection<SearchCollectionSource, SearchResult> SearchResults { get; set; }

        public RelayCommand SearchCommand => new RelayCommand(ResetSearch);

        public override Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            if (eventArgs.NavigationMode != NavigationServiceNavigationMode.Back)
            {
                SearchType = SearchTypes.First(x => x == SearchType.Photos);
            }

            return base.OnNavigatedToAsync(eventArgs);
        }

        //[UsedImplicitly]
        //private void OnSearchTermChanged()
        //{
        //    ResetSearch();
        //}

        private void ResetSearch()
        {
            SearchResults = new ServiceBasedIncrementalLoadingCollection<SearchCollectionSource, SearchResult>(Services);
            SearchResults.SourceCollection.SetSearchOptions(SearchTerm, SearchType, Category);
        }
    }

    public enum SearchType
    {
        Photos,
        Users
    }

    public enum SearchCategory 
    {
        
    }

    public class SearchResult
    {
        public SearchResult(Photo photo, IServices services)
        {
            Photo = new PhotoViewModel(photo, services);
        }

        public SearchResult(User user, IServices services)
        {
            User = new UserViewModel(user, services);
        }

        public PhotoViewModel Photo { get; set; }
        public UserViewModel User { get; set; }
    }
}
