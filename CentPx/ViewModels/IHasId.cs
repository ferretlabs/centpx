﻿namespace CentPx.ViewModels
{
    public interface IHasId
    {
        string Id { get; }
    }
}
