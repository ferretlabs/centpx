﻿using CentPx.Client;
using CentPx.Core.Services;
using CentPx.Services;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using ScottIsAFool.Windows.MvvmLight.Extensions;

namespace CentPx.ViewModels
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            //if (ViewModelBase.IsInDesignModeStatic)
            //{

            //}
            //else
            //{
                
            //}

            SimpleIoc.Default.RegisterIf<ICentPxClient>(() => new CentPxClient("svp8iRzpZUOoB8EVsqH8pmJ0sbkIavokLtQfey4O", "LsIQ7cV0pjJZ8GRK10Qv0KU4ROH9LPwTVAdwoea6"));
            SimpleIoc.Default.RegisterIf<INavigationService>(() => new NavigationService());
            SimpleIoc.Default.RegisterIf<IStorageService, StorageService>();
            SimpleIoc.Default.RegisterIf<IDispatcherService, DispatcherService>();
            SimpleIoc.Default.RegisterIf<IMessageBoxService, MessageBoxService>();
            SimpleIoc.Default.RegisterIf<IApplicationSettingsService, ApplicationSettingsService>();
            SimpleIoc.Default.RegisterIf<ILauncherService, LauncherService>();
            SimpleIoc.Default.RegisterIf<IAuthenticationService, AuthenticationService>();
            SimpleIoc.Default.RegisterIf<IThemeService, ThemeService>();
            SimpleIoc.Default.RegisterIf<IAnimationService, AnimationService>();
            SimpleIoc.Default.RegisterIf<IServices, ServicesContainer>();
            SimpleIoc.Default.RegisterIf<IFilePickerService, FilePickerService>();

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<HomeViewModel>();
            SimpleIoc.Default.Register<UserProfileViewModel>();
            SimpleIoc.Default.Register<PhotoViewerViewModel>();
            SimpleIoc.Default.Register<UploadViewModel>();
            SimpleIoc.Default.Register<AuthenticatedUserViewModel>();
            SimpleIoc.Default.Register<SearchViewModel>();
        }

        public MainViewModel Main => ServiceLocator.Current.GetInstance<MainViewModel>();
        public LoginViewModel Login => ServiceLocator.Current.GetInstance<LoginViewModel>();
        public HomeViewModel Home => ServiceLocator.Current.GetInstance<HomeViewModel>();
        public UserProfileViewModel UserProfile => ServiceLocator.Current.GetInstance<UserProfileViewModel>();
        public PhotoViewerViewModel PhotoViewer => ServiceLocator.Current.GetInstance<PhotoViewerViewModel>();
        public UploadViewModel Upload => ServiceLocator.Current.GetInstance<UploadViewModel>();
        public AuthenticatedUserViewModel AuthenticatedUser => ServiceLocator.Current.GetInstance<AuthenticatedUserViewModel>();
        public SearchViewModel Search => ServiceLocator.Current.GetInstance<SearchViewModel>();
    }
}
