﻿using CentPx.Services;
using CentPx.ViewModels.Item;

namespace CentPx.ViewModels
{
    public class AuthenticatedUserViewModel : ViewModelBase
    {
        public AuthenticatedUserViewModel(IServices services) : base(services)
        {
            services.Authentication.LoginStatusChanged += (sender, args) =>
            {
                SetUser();
            };

            SetUser();
        }

        private void SetUser()
        {
            User = Authentication.IsSignedIn ? new UserViewModel(Authentication.SignedInUser, Services) : null;
        }

        public UserViewModel User { get; set; }
    }
}
