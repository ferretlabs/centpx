﻿using System.Threading.Tasks;
using CentPx.Services;
using Cimbalino.Toolkit.Handlers;
using Cimbalino.Toolkit.Services;

namespace CentPx.ViewModels
{
    public abstract class BasePageViewModel : ViewModelBase, IHandleNavigatedTo, IHandleNavigatedFrom
    {
        public virtual Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            return Task.FromResult(0);
        }

        public virtual Task OnNavigatedFromAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            return Task.FromResult(0);
        }

        protected BasePageViewModel(IServices services) : base(services)
        {
        }
    }
}