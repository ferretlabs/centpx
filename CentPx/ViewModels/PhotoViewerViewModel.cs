﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CentPx.Client.Exceptions;
using CentPx.Client.Model.Types;
using CentPx.Core.Extensions;
using CentPx.Services;
using CentPx.ViewModels.Item;
using Cimbalino.Toolkit.Services;

namespace CentPx.ViewModels
{
    public class PhotoViewerViewModel : BasePageViewModel
    {
        public PhotoViewerViewModel(IServices services) : base(services)
        {
        }

        public PhotoViewModel Photo { get; set; }

        public ObservableCollection<object> Comments { get; set; }

        public override Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            Photo = null;
            Comments = null;

            var photo = eventArgs.Parameter as PhotoViewModel;
            if (photo != null)
            {
                Photo = photo;
            }

            UpdatePhotoDetails().DontAwait("Update this silently");

            return base.OnNavigatedToAsync(eventArgs);
        }

        private async Task UpdatePhotoDetails()
        {
            try
            {
                var photo = await CentPx.GetPhoto(Photo.Photo.Id, ImageSize.Long2048, true, includeTags: true);
                if (photo != null)
                {
                    Photo.ReloadPhoto(photo.Photo);
                }
            }
            catch (CentPxException)
            {
                
            }
        }
    }
}
