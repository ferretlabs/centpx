﻿using CentPx.Client;
using CentPx.Core.Services;
using CentPx.Services;
using Cimbalino.Toolkit.Services;

namespace CentPx.ViewModels
{
    public abstract class ViewModelBase : GalaSoft.MvvmLight.ViewModelBase
    {
        protected readonly IServices Services;
        //public ILog Log { get; set; }

        protected ViewModelBase(IServices services)
        {
            Services = services;
            if (!IsInDesignMode)
            {
                WireMessages();
                //Log = new WinLogger(GetType().FullName);
            }
        }

        protected virtual void WireMessages()
        {
        }

        public void SetProgressBar(string text)
        {
            ProgressIsVisible = true;
            ProgressText = text;

            UpdateProperties();
        }

        public void SetProgressBar()
        {
            ProgressIsVisible = false;
            ProgressText = string.Empty;

            UpdateProperties();
        }

        public bool ProgressIsVisible { get; set; }
        public string ProgressText { get; set; }

        protected INavigationService Navigation => Services.Navigation;
        protected IAuthenticationService Authentication => Services.Authentication;
        protected ICentPxClient CentPx => Services.CentPx;
        protected IDispatcherService Dispatcher => Services.Dispatcher;

        public virtual void UpdateProperties() { }
    }
}
