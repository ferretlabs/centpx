using System.Threading.Tasks;
using CentPx.Services;
using CentPx.Views;
using Cimbalino.Toolkit.Services;

namespace CentPx.ViewModels
{
    public class MainViewModel : BasePageViewModel
    {
        private readonly IDispatcherService _dispatcherService;

        public MainViewModel(
            IServices services,
            IDispatcherService dispatcherService) : base(services)
        {
            _dispatcherService = dispatcherService;
        }

        public override async Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            await _dispatcherService.InvokeOnUiThreadAsync(async () =>
            {
                await Task.Delay(200);
                if (Authentication.IsSignedIn)
                {
                    Navigation.Navigate<SearchView>();
                }
                else
                {
                    Navigation.Navigate<LoginView>();
                }

                Navigation.RemoveBackEntry();
            });
        }
    }
}