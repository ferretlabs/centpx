﻿using System.Diagnostics;
using System.Linq;
using CentPx.Client.Extensions;
using CentPx.Client.Model;
using CentPx.Client.Model.Types;
using CentPx.Core.Extensions;
using CentPx.Extensions;
using CentPx.Model;
using CentPx.Services;
using CentPx.Views;
using GalaSoft.MvvmLight.Command;

namespace CentPx.ViewModels.Item
{
    [DebuggerDisplay("Name: {Name}")]
    public class PhotoViewModel : ViewModelBase, IHasId
    {
        private const int ThumbnailHeightConst = 320;
        public const string PhotoAnimationKey = "PhotoAnimation";
        public Photo Photo { get; set; }

        public PhotoViewModel(Photo photo, IServices services) : base(services)
        {
            Photo = photo;
            User = new UserViewModel(photo.User, services);
        }

        public string Id => Photo.Id.ToString();
        public string Name => Photo.Name;
        public string Description => Photo.Description?.Replace("\n", "<br/>");
        public bool IsLiked => Photo.IsLiked;
        public string Likes => Photo.PositiveVotesCount.ToString();
        public double Rating => Photo.HighestRating ?? 0.0d;
        public string RatingDate => Photo.HighestRatingDate?.ToShortDate();
        public bool IsPrivate => Photo.Privacy;
        public int Views => Photo.TimesViewed;
        public string Feature => Photo.Feature?.GetFeatureName();
        public CameraInfo CameraInfo => Photo.GetCameraInfo();
        public bool IsNsfw => Photo.Nsfw;
        public bool HasComments => Photo.CommentsCount > 0;
        public string CommentCount => Photo.CommentsCount.ToString();

        public string UploadedOn => Photo.CreatedAt?.ToShortDate();
        public string UploadedOnRelative => Photo.CreatedAt?.ToRelativeTime();
        public string TakenOn => Photo.TakenAt?.ToShortDate();
        public string TakenOnRelative => Photo.TakenAt?.ToRelativeTime();
        public string Category => Photo.Category.GetCategoryName();

        public string FullResImageUrl => Photo.Images?.FirstOrDefault(x => x.Size == ImageSize.Long2048)?.HttpsUrl;
        public string ImageUrl => Photo.Images?.FirstOrDefault(x => x.Size != ImageSize.Long2048)?.HttpsUrl ?? Photo.Images?.FirstOrDefault()?.HttpsUrl;

        public string WebLink => $"https://www.500px.com/{Photo.Url}";

        public UserViewModel User { get; set; }

        public int ThumbnailHeight { get; } = ThumbnailHeightConst;
        public int ThumbnailWidth => GetThumbnailWidth(Photo);

        public void ReloadPhoto(Photo photo)
        {
            Photo = photo;
        }

        private int GetThumbnailWidth(Photo photo)
        {
            var ratio = photo.Width/(double) photo.Height;
            return (int) (ratio*ThumbnailHeight);
        }

        public RelayCommand LikeUnlikeCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    var task = IsLiked ? CentPx.UnlikePhoto(Photo.Id) : CentPx.LikePhoto(Photo.Id);
                    //var result = await task;
                    //if (result != null)
                    //{
                    //    Photo.VotesCount = result.VotesCount;
                    //    Photo.IsLiked = result.IsLiked;
                    //    RaisePropertyChanged(() => IsLiked);
                    //    RaisePropertyChanged(() => Likes);
                    //}
                });
            }
        }

        public RelayCommand<object> NavigateToPhoto
        {
            get
            {
                return new RelayCommand<object>(thumbnail =>
                {
                    if (thumbnail != null)
                    {
                        Services.Animation.PrepareToAnimate(PhotoAnimationKey, thumbnail);
                    }

                    Navigation.Navigate<PhotoViewerView>(this);
                });
            }
        }
    }
}
