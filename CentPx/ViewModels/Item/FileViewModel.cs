﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using CentPx.Client.Exceptions;
using CentPx.Client.Model;
using CentPx.Client.Model.Requests;
using CentPx.Client.Model.Types;
using CentPx.Core.Extensions;
using CentPx.Messages;
using CentPx.Model;
using CentPx.Services;
using CompositionProToolkit;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace CentPx.ViewModels.Item
{
    public class FileViewModel : ViewModelBase
    {
        public ImageFile ImageFile { get; set; }

        public FileViewModel(ImageFile file, IServices services)
            : base(services)
        {
            ImageFile = file;

            if (ImageFile != null)
            {
                SetThumbnail().DontAwait("Just crack on");
                Title = ImageFile.Name;
            }
        }

        private async Task SetThumbnail()
        {
            //var bi = new BitmapImage();
            //bi.SetSource(await ImageFile.ReadStreamAsync());
            //BitmapImage = bi;

            var uri = await ImageCache.GetCachedUriAsync(ImageFile.GetInternalFile());
            ImageUri = uri;
        }

        public BitmapImage BitmapImage { get; set; }
        public Uri ImageUri { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public CategoryTypes Category { get; set; }
        public LicenseTypes License { get; set; }
        public string Keywords { get; set; }
        public bool IsAdult { get; set; }
        public bool IsPrivate { get; set; }

        public bool IsDummyType { get; set; }

        public RelayCommand RemovePhotoCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    Messenger.Default.Send(new RemoveFileMessage(this));
                });
            }
        }

        public async Task UploadPhoto(CancellationToken cancellationToken)
        {
            try
            {
                SetProgressBar("Uploading photo...");
                var options = await PreparePhotoOptionsForUpload();
                var photo = await CentPx.UploadPhoto((await ImageFile.ReadStreamAsync()).AsStreamForRead(), ImageFile.Name, options.Item1, cancellationToken);

                if (photo != null)
                {
                    await CentPx.UpdatePhotoDetails(photo.Id, options.Item2, cancellationToken);
                }
            }
            catch (CentPxException ex)
            {
                var i = 1;
            }
            finally
            {
                SetProgressBar();
            }
        }

        private async Task<Tuple<UploadPhotoOptions, UpdatePhotoOptions>> PreparePhotoOptionsForUpload()
        {
            var uploadOptions = new UploadPhotoOptions();
            var updateOptions = new UpdatePhotoOptions();
            var photoInfo = await PhotoInfo.GetAndCreatePhotoInfo(ImageFile);
            var imageProps = ImageFile.ImageProperties;
            var tags = Keywords?.Split(',')?.Select(x => x.Trim()).ToList();

            uploadOptions.Name = Title;
            uploadOptions.Description = Description;
            uploadOptions.Category = Category;
            uploadOptions.ShutterSpeed = photoInfo.Exposure;
            uploadOptions.FocalLength = photoInfo.FocalLength;
            uploadOptions.Aperture = photoInfo.FStop;
            uploadOptions.Iso = photoInfo.Iso;
            uploadOptions.Camera = CheckCameraMakeModel();
            uploadOptions.Lens = photoInfo.LensSpecification;
            uploadOptions.Latitude = imageProps.Latitude;
            uploadOptions.Longitude = imageProps.Longitude;
            uploadOptions.IsPrivate = IsPrivate;
            uploadOptions.Tags = tags;

            updateOptions.IsNsfw = IsAdult;
            updateOptions.License = License;
            updateOptions.TagsToAdd = tags;

            return new Tuple<UploadPhotoOptions, UpdatePhotoOptions>(uploadOptions, updateOptions);
        }

        private string CheckCameraMakeModel()
        {
            var cameraMake = ImageFile.ImageProperties.CameraManufacturer;
            var cameraModel = ImageFile.ImageProperties.CameraModel;

            if (string.IsNullOrEmpty(cameraMake) && string.IsNullOrEmpty(cameraModel))
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(cameraModel))
            {
                return string.Empty;
            }

            if (cameraModel.Contains(cameraMake))
            {
                return cameraModel;
            }

            return $"{cameraMake} {cameraModel}";
        }
    }
}
