using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentPx.Client.Model;
using CentPx.Extensions;
using CentPx.Model;
using CentPx.Services;
using CentPx.Views;
using GalaSoft.MvvmLight.Command;

namespace CentPx.ViewModels.Item
{
    public class UserViewModel : ViewModelBase, IHasId
    {
        public User User { get; set; }

        public UserViewModel(User user, IServices services) : base(services)
        {
            User = user;
        }

        public string Id => User.Id.ToString();
        public string FirstName => User.Firstname;
        public string LastName => User.Lastname;
        public string FullName => !string.IsNullOrEmpty(User.Fullname) ? User.Fullname : Username;

        public string ProfilePicture => DetermineProfilePicture(User);

        public string CoverPhoto => User.CoverUrl;
        public bool HasCoverPhoto => !string.IsNullOrEmpty(CoverPhoto);
        public string About => User.About;
        public string AboutShort => ShortenAbout(About);

        public string Username => User.Username;
        public string Place => SetPlace(User);

        public bool HasStore => User.StoreOn;
        public string StoreUrl => $"https://marketplace.500px.com/{Username}";

        public string TotalFollowers => User.FollowersCount == 1 ? "1 follower" : $"{User.FollowersCount:N0} followers";
        public string TotalAffection => $"{User.Affection:N0} affection";
        public string TotalFollowing => User.FriendsCount == 1 ? "1 friend" : $"{User.FriendsCount:N0} friends";
        public string TotalPhotos => User.PhotosCount == 1 ? "1 photo" : $"{User.PhotosCount:N0} photos";

        public List<CameraInfo> Equipment => User.GetEquipment();
        public bool HasEquipment => Equipment != null && Equipment.Any();

        public bool AllowNsfw => User.ShowNude;

        public bool IsFollowing => User.Following;
        public bool IsChangingFollowingState { get; set; }

        public void ReloadUser(User user)
        {
            User = user;
        }

        public RelayCommand NavigateToUserCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    Navigation.Navigate<UserProfileView>(this);
                });
            }
        }

        public RelayCommand OpenUserLocationCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    if (string.IsNullOrWhiteSpace(Place))
                    {
                        return;
                    }

                    var mapsUrl = $"bingmaps:?where={Place}";

                    Services.Launcher.LaunchUriAsync(mapsUrl);
                });
            }
        }

        public RelayCommand FollowUnfollowCommand
        {
            get
            {
                return new RelayCommand(async () =>
                {
                    IsChangingFollowingState = true;
                    
                    await Task.Delay(6000);

                    IsChangingFollowingState = false;

                    User.Following = !User.Following;
                    RaisePropertyChanged(() => IsFollowing);
                });
            }
        }

        private static string SetPlace(User user)
        {
            var place = user.City;
            if (!string.IsNullOrEmpty(place))
            {
                place += ", ";
            }

            if (!string.IsNullOrEmpty(user.State))
            {
                place += $"{user.State}, ";
            }

            place += user.Country;

            return place;
        }

        private static string DetermineProfilePicture(User user)
        {
            var profilePicture = "ms-appx:///Assets/DefaultUser.png";
            if (!string.IsNullOrEmpty(user.Avatars?.Large?.Https))
            {
                profilePicture = user.Avatars.Large.Https;
            }
            else if (!string.IsNullOrEmpty(user.UserpicUrl))
            {
                profilePicture = user.UserpicUrl;
            }

            return profilePicture;
        }

        private static string ShortenAbout(string about)
        {
            if (string.IsNullOrEmpty(about))
            {
                return about;
            }

            const int maxChars = 150;
            if (about.Length <= maxChars)
            {
                return about;
            }

            return about.Substring(0, maxChars) + "...";
        }

    }
}