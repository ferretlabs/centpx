﻿using System.Linq;
using System.Threading.Tasks;
using CentPx.Client.Exceptions;
using CentPx.Collections;
using CentPx.Services;
using CentPx.ViewModels.Item;
using CentPx.Views;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Command;

namespace CentPx.ViewModels
{
    public class HomeViewModel : BasePageViewModel
    {
        public HomeViewModel(IServices services) : base(services)
        {
        }

        public RelayCommand NavigateToUploadCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    Navigation.Navigate<UploadView>();
                });
            }
        }

        public RelayCommand NavigateToSearchCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    Navigation.Navigate<SearchView>();
                });
            }
        }

        public RelayCommand DoSomethingCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    try
                    {
                        Authentication.SignOut();
                        Navigation.Navigate<LoginView>();

                        Navigation.ClearBackstack();
                    }
                    catch (CentPxException ex)
                    {
                        var i = 0;
                    }
                });
            }
        }

        public IncrementalLoadingCollectionWithId<UserFollowingPhotoCollection, PhotoViewModel> FriendActivityPhotos { get; set; }

        public override Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            if (eventArgs.NavigationMode != NavigationServiceNavigationMode.Back)
            {
                LoadFriendActivity(false);
            }

            return base.OnNavigatedToAsync(eventArgs);
        }

        private void LoadFriendActivity(bool isRefresh)
        {
            if (!isRefresh && FriendActivityPhotos != null && FriendActivityPhotos.Any())
            {
                return;
            }

            try
            {
                SetProgressBar("Loading friend activity...");

                if (FriendActivityPhotos == null || isRefresh)
                {
                    FriendActivityPhotos = new IncrementalLoadingCollectionWithId<UserFollowingPhotoCollection, PhotoViewModel>(Services);
                }

                FriendActivityPhotos.SetId(Authentication.SignedInUser.Id);

            }
            catch (CentPxException)
            {

            }
            finally
            {
                SetProgressBar();
            }
        }
    }
}
