﻿using System.Threading.Tasks;
using CentPx.Services;
using CentPx.Views;
using Cimbalino.Toolkit.Services;
using GalaSoft.MvvmLight.Command;

namespace CentPx.ViewModels
{
    public class LoginViewModel : BasePageViewModel
    {
        private readonly IThemeService _themeService;

        public LoginViewModel(
            IServices services,
            IThemeService themeService) : base(services)
        {
            _themeService = themeService;
        }

        public ThemeItem Theme { get; set; }

        public RelayCommand LoginCommand
        {
            get
            {
                return new RelayCommand(async () =>
                {
                    await Authentication.Authenticate();
                    if (Authentication.IsSignedIn)
                    {
                        Navigation.Navigate<HomeView>();
                        Navigation.ClearBackstack();
                    }
                });
            }
        }

        public override Task OnNavigatedToAsync(NavigationServiceNavigationEventArgs eventArgs)
        {
            var theme = _themeService.GetTheme();
            Theme = theme;
            return Task.FromResult(0);
        }
    }
}
