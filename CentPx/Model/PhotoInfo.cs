﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentPx.Client.Extensions;
using CentPx.Extensions;
using ExifLib;

namespace CentPx.Model
{
    public class PhotoInfo
    {
        private const string SystemBase = "System.Photo.";
        private const string ApertureKey = SystemBase + "Aperture";
        private const string ExposureKey = SystemBase + "ExposureTime";
        private const string FNumberKey = SystemBase + "FNumber";
        private const string FocalLengthKey = SystemBase + "FocalLength";
        private const string LensKey = SystemBase + "LensModel";
        private const string IsoKey = SystemBase + "ISOSpeed";
        private const string LensManufacturerKey = SystemBase + "LensManufacturer";

        private static readonly List<string> Requests = new List<string>
        {
            ApertureKey,
            ExposureKey,
            FNumberKey,
            FocalLengthKey,
            LensKey,
            IsoKey,
            LensManufacturerKey,
        };

        public static async Task<PhotoInfo> GetAndCreatePhotoInfo(ImageFile imageFile)
        {
            var props = await imageFile.ImageProperties.RetrievePropertiesAsync(Requests);
            var imageProps = imageFile.ImageProperties;
            var details = props.ToList();
            var photoInfo = new PhotoInfo(details)
            {
                CameraMake = imageProps.CameraManufacturer,
                CameraModel = imageProps.CameraModel,
                DateTaken = imageProps.DateTaken
            };

            using (var exifReader = await imageFile.GetExifReader())
            {
                double[] lens;
                if (exifReader.GetTagValue(ExifTags.LensSpecification, out lens))
                {
                    var minLength = lens[0];
                    var maxLength = lens[1];

                    var spec = minLength == maxLength ? $"{minLength}mm" : $"{minLength}-{maxLength}mm";

                    photoInfo.LensSpecification = spec;
                }
            }

            return photoInfo;
        }

        private readonly List<KeyValuePair<string, object>> _properties;
        private PhotoInfo(List<KeyValuePair<string, object>> properties)
        {
            _properties = properties;

            GetValues();
        }

        public string FStop { get; private set; }
        public string Exposure { get; private set; }
        public int Iso { get; private set; }
        public string LensMake { get; private set; }
        public string LensModel { get; private set; }
        public string FocalLength { get; private set; }
        public string CameraMake { get; private set; }
        public string CameraModel { get; private set; }
        public DateTimeOffset? DateTaken { get; private set; }
        public string LensSpecification { get; private set; }

        private void GetValues()
        {
            if (_properties == null || !_properties.Any())
            {
                return;
            }

            var fstop = _properties.GetValue<double>(FNumberKey);
            FStop = $"{fstop}";

            var focalLength = _properties.GetValue<double>(FocalLengthKey);
            FocalLength = $"{focalLength}mm";

            var lensMake = _properties.GetValue<string>(LensManufacturerKey);
            LensMake = lensMake;

            var lensModel = _properties.GetValue<string>(LensKey);
            LensModel = lensModel;

            var iso = _properties.GetValue<ushort>(IsoKey);
            Iso = iso;

            var exposure = _properties.GetValue<double>(ExposureKey);
            Exposure = exposure.ToFraction()?.Trim();
        }
    }
}