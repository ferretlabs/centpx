﻿namespace CentPx.Model
{
    public class CameraInfo
    {
        public CameraInfo(string name, string lens, string focalLength, string iso, string aperture)
        {
            Name = name;
            Lens = lens;
            FocalLength = focalLength;
            Iso = iso;
            Aperture = aperture;
        }

        public string Name { get; }
        public string Lens { get; }
        public string FocalLength { get; }
        public string Iso { get; }
        public string Aperture { get; }

        public bool DisplayCameraInfo => !string.IsNullOrEmpty(Name);
    }
}