﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using CentPx.Extensions;
using Cimbalino.Toolkit.Services;
using ExifLib;

namespace CentPx.Model
{
    public static class FileExtensions
    {
        public static StorageFile GetInternalFile(this File file)
        {
            return file.StorageFile;
        }
    }

    public class File
    {
        internal readonly StorageFile StorageFile;

        public File(StorageFile storageFile)
        {
            StorageFile = storageFile;
        }

        public File(FilePickerServiceFileResult result)
        {
            StorageFile = result.GetStorageFile();
        }

        public string Name => StorageFile.DisplayName;
        public string FileType => StorageFile.FileType;

        public Task<IRandomAccessStream> ReadStreamAsync()
        {
            return StorageFile.OpenAsync(FileAccessMode.Read).AsTask();
        }
    }

    public class ImageFile : File
    {
        public ImageFile(StorageFile storageFile)
            : base(storageFile)
        {
        }

        public ImageFile(FilePickerServiceFileResult result)
            : base(result)
        {
        }

        public ImageProperties ImageProperties { get; set; }

        public async Task<ExifReader> GetExifReader()
        {
            var stream = (await ReadStreamAsync()).AsStreamForRead();
                var reader = new ExifReader(stream);
                return reader;
        }
    }
}