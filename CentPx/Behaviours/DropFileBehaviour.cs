﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage;
using Windows.UI.Xaml;
using CentPx.Model;
using Microsoft.Xaml.Interactivity;

namespace CentPx.Behaviours
{
    public class DropFileBehaviour : Behavior<UIElement>
    {
        private string[] _fileTypes = { ".jpg", ".jpeg", ".png", ".cr2" };

        public static readonly DependencyProperty SupportedFileTypesProperty = DependencyProperty.Register(
            nameof(SupportedFileTypes), typeof(string), typeof(DropFileBehaviour), new PropertyMetadata(default(string), OnSupportedFileTypesChanged));

        public string SupportedFileTypes
        {
            get { return (string) GetValue(SupportedFileTypesProperty); }
            set { SetValue(SupportedFileTypesProperty, value); }
        }

        public static readonly DependencyProperty DroppedCommandProperty = DependencyProperty.Register(
            nameof(DroppedCommand), typeof(ICommand), typeof(DropFileBehaviour), new PropertyMetadata(default(ICommand)));

        public ICommand DroppedCommand
        {
            get { return (ICommand) GetValue(DroppedCommandProperty); }
            set { SetValue(DroppedCommandProperty, value); }
        }

        public static readonly DependencyProperty IsDroppingProperty = DependencyProperty.Register(
            nameof(IsDropping), typeof(bool), typeof(DropFileBehaviour), new PropertyMetadata(default(bool)));

        public bool IsDropping
        {
            get { return (bool) GetValue(IsDroppingProperty); }
            set { SetValue(IsDroppingProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.AllowDrop = true;

            UnregisterEvents();

            AssociatedObject.Drop += AssociatedObjectOnDrop;
            AssociatedObject.DragEnter += AssociatedObjectOnDragEnter;
            AssociatedObject.DragLeave += AssociatedObjectOnDragLeave;
        }

        private void UnregisterEvents()
        {
            AssociatedObject.Drop -= AssociatedObjectOnDrop;
            AssociatedObject.DragEnter -= AssociatedObjectOnDragEnter;
            AssociatedObject.DragLeave -= AssociatedObjectOnDragLeave;
        }

        private void AssociatedObjectOnDragLeave(object sender, DragEventArgs dragEventArgs)
        {
            IsDropping = false;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            UnregisterEvents();
        }

        private static void OnSupportedFileTypesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as DropFileBehaviour)?.SetFileTypes();
        }

        private void AssociatedObjectOnDragEnter(object sender, DragEventArgs e)
        {
            IsDropping = true;
            e.AcceptedOperation = DataPackageOperation.Copy;
        }

        private void SetFileTypes()
        {
            var fileTypes = SupportedFileTypes?.Split(',');
            _fileTypes = fileTypes;
        }

        private async void AssociatedObjectOnDrop(object sender, DragEventArgs e)
        {
            var files = await e.DataView.GetStorageItemsAsync();
            var tasks = files.Cast<StorageFile>().Select(GetFileInfo);

            var images = await Task.WhenAll(tasks);
            var noNullImages = images.Where(x => x != null).ToList();

            DroppedCommand?.Execute(noNullImages);

            IsDropping = false;
        }

        private async Task<File> GetFileInfo(StorageFile file)
        {
            try
            {
                if (_fileTypes == null || _fileTypes.Contains(file.FileType))
                {
                    var properties = await file.Properties.GetImagePropertiesAsync();
                    return new ImageFile(file) {ImageProperties = properties};
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return null;
        }
    }
}
