﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using CentPx.ViewModels.Item;
using Microsoft.Xaml.Interactivity;

namespace CentPx.Behaviours
{
    public class MultiSelectBehaviorBase<T> : Behavior<ListViewBase>
    {
        #region SelectedItems Attached Property
        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register(
            nameof(SelectedItems),
            typeof(ObservableCollection<T>),
            typeof(MultiSelectBehaviorBase<T>),
            new PropertyMetadata(new ObservableCollection<T>(), PropertyChangedCallback));

        #endregion

        #region private
        private bool _selectionChangedInProgress; // Flag to avoid infinite loop if same viewmodel is shared by multiple controls
        #endregion

        public MultiSelectBehaviorBase()
        {
            SelectedItems = new ObservableCollection<T>();
        }

        public ObservableCollection<T> SelectedItems
        {
            get { return (ObservableCollection<T>)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += OnSelectionChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.SelectionChanged -= OnSelectionChanged;
            
            // unhook the collection notifications if wired up too.
            if (SelectedItems != null)
            {
                SelectedItems.CollectionChanged -= SelectedItemsChanged;
            }
        }

        private static void PropertyChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            MultiSelectBehaviorBase<T> msbSender = sender as MultiSelectBehaviorBase<T>;
            msbSender?.WireNewSelectedItems(args.OldValue as ObservableCollection<T>, args.NewValue as ObservableCollection<T>);
        }

        private void WireNewSelectedItems(INotifyCollectionChanged oldValue, ObservableCollection<T> newValue)
        {
            if (oldValue != null)
            {
                oldValue.CollectionChanged -= SelectedItemsChanged;
            }

            if (newValue != null)
            {
                var listViewBase = AssociatedObject;
                if (listViewBase != null)
                {
                    var listSelectedItems = listViewBase.SelectedItems;
                    bool bOldInProgress = _selectionChangedInProgress;
                    _selectionChangedInProgress = true;   // don't notify, as we're in control

                    listSelectedItems.Clear();
                    foreach (var v in newValue)
                    {
                        listSelectedItems.Add(v);
                    }

                    _selectionChangedInProgress = bOldInProgress;
                }

                // wire up notifications on the new collection
                newValue.CollectionChanged += SelectedItemsChanged; ;
            }
        }

        private void SelectedItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var listViewBase = AssociatedObject;

            var listSelectedItems = listViewBase.SelectedItems;
            if (e.Action == NotifyCollectionChangedAction.Reset)
                listSelectedItems.Clear();
            else
            {
                if (e.OldItems != null)
                {
                    foreach (var item in e.OldItems)
                    {
                        if (listSelectedItems.Contains(item))
                        {
                            listSelectedItems.Remove(item);
                        }
                    }
                }

                if (e.NewItems != null)
                {
                    foreach (var item in e.NewItems)
                    {
                        if (!listSelectedItems.Contains(item))
                        {
                            listSelectedItems.Add(item);
                        }
                    }
                }
            }
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_selectionChangedInProgress) return;
            _selectionChangedInProgress = true;
            foreach (T item in e.RemovedItems)
            {
                if (SelectedItems.Contains(item))
                {
                    SelectedItems.Remove(item);
                }
            }

            foreach (T item in e.AddedItems)
            {
                if (!SelectedItems.Contains(item))
                {
                    SelectedItems.Add(item);
                }
            }
            _selectionChangedInProgress = false;
        }
    }

    public class MultiSelectFilesBehavior : MultiSelectBehaviorBase<FileViewModel> { }
}
