﻿using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Microsoft.Xaml.Interactivity;

namespace CentPx.Behaviours
{
    /// <summary>
    /// Helps resize a control to fill its parent.
    /// Provides a Stretch=UniformToFill behavior.
    /// </summary>
    public class FillParentBehavior : Behavior<FrameworkElement>
    {
        /// <summary>
        /// The parent element to measure.
        /// </summary>
        private FrameworkElement _parent;

        public static readonly DependencyProperty ParentProperty = DependencyProperty.Register(
            nameof(Parent), typeof(FrameworkElement), typeof(FillParentBehavior), new PropertyMetadata(default(UIElement), OnParentChanged));

        public FrameworkElement Parent
        {
            get { return (FrameworkElement) GetValue(ParentProperty); }
            set { SetValue(ParentProperty, value); }
        }

        private static void OnParentChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var behaviour = sender as FillParentBehavior;
            behaviour?.ResetStuff(e.OldValue as FrameworkElement, e.NewValue as FrameworkElement);
        }

        private void ResetStuff(FrameworkElement old, FrameworkElement parent)
        {
            if (old != null)
            {
                old.SizeChanged -= OnParentSizeChanged;
            }

            _parent = parent;
            if (_parent != null)
            {
                _parent.SizeChanged += OnParentSizeChanged;
                OnParentSizeChanged(null, null);
            }
        }

        /// <summary>
        /// Called when the behavior is being detached from its AssociatedObject, but before it has actually occurred.
        /// </summary>
        /// <remarks>
        /// Override this to unhook functionality from the AssociatedObject.
        /// </remarks>
        protected override void OnDetaching()
        {
            base.OnDetaching();

            if (_parent != null)
            {
                _parent.SizeChanged -= OnParentSizeChanged;
            }
        }

        /// <summary>
        /// Called when [parent size changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs"/> instance containing the event data.</param>
        private void OnParentSizeChanged(object sender, SizeChangedEventArgs e)
        {
            double width = AssociatedObject.Width;
            double height = AssociatedObject.Height;
            var parentSize = new Size(_parent.ActualWidth, _parent.ActualHeight);
            var parentRatio = parentSize.Width / parentSize.Height;

            // determine optimal size
            if (AssociatedObject is Image)
            {
                var image = (Image)AssociatedObject;
                if (image.Source is BitmapImage)
                {
                    width = AdjustWidth((BitmapImage) image.Source, parentRatio, width, parentSize, ref height);
                }
            }

            AssociatedObject.Width = width;
            AssociatedObject.Height = height;
        }

        private static double AdjustWidth(BitmapImage image, double parentRatio, double width, Size parentSize, ref double height)
        {
            var bitmap = image;
            var imageSize = new Size(bitmap.PixelWidth, bitmap.PixelHeight);
            var imageRatio = imageSize.Width/imageSize.Height;
            if (parentRatio <= imageRatio)
            {
                // picture has a greater width than necessary
                // use its height
                width = double.NaN;
                height = parentSize.Height;
            }
            else
            {
                // picture has a greater height than necessary
                // use its width
                width = parentSize.Width;
                height = double.NaN;
            }
            return width;
        }
    }
}