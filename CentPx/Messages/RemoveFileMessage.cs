﻿using CentPx.ViewModels.Item;
using GalaSoft.MvvmLight.Messaging;

namespace CentPx.Messages
{
    public class RemoveFileMessage : MessageBase
    {
        public FileViewModel File { get; set; }
        public RemoveFileMessage(FileViewModel file)
        {
            File = file;
        }
    }
}
