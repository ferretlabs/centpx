﻿using System.Collections.Generic;
using System.Linq;
using CentPx.Client.Model;
using CentPx.Model;

namespace CentPx.Extensions
{
    public static class UserExtensions
    {
        public static List<CameraInfo> GetEquipment(this User user)
        {
            return user.Equipment?.Camera?.Select(x => new CameraInfo(x, string.Empty, string.Empty, string.Empty, string.Empty)).ToList() ?? new List<CameraInfo>();
        }
    }
}