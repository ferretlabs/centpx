﻿using System.Reflection;
using Windows.Storage;
using Cimbalino.Toolkit.Services;

namespace CentPx.Extensions
{
    public static class FilePickerServiceExtensions
    {
        public static StorageFile GetStorageFile(this FilePickerServiceFileResult result)
        {
            var field = result.GetType().GetField("_storageFile", BindingFlags.Instance | BindingFlags.NonPublic);
            return field.GetValue(result) as StorageFile;
        }
    }
}
