﻿using CentPx.Client.Model;
using CentPx.Model;

namespace CentPx.Extensions
{
    public static class PhotoExtensions
    {
        public static CameraInfo GetCameraInfo(this Photo photo)
        {
            return new CameraInfo(photo.Camera, photo.Lens, photo.FocalLength, photo.Iso, photo.Aperture);
        }
    }
}
