﻿using CentPx.ViewModels;

namespace CentPx.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UploadView
    {
        private UploadViewModel Upload => DataContext as UploadViewModel;
        public static UploadView Current { get; set; }

        public UploadView()
        {
            Current = this;
            InitializeComponent();
        }
    }
}
