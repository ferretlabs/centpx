﻿using CentPx.ViewModels;

namespace CentPx.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SearchView
    {
        private SearchViewModel Search => DataContext as SearchViewModel;

        public SearchView()
        {
            InitializeComponent();
        }
    }
}
