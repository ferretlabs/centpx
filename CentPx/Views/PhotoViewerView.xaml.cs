﻿using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using CentPx.Services;
using CentPx.ViewModels;
using CentPx.ViewModels.Item;
using GalaSoft.MvvmLight.Ioc;

namespace CentPx.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PhotoViewerView
    {
        private PhotoViewerViewModel PhotoViewer => DataContext as PhotoViewerViewModel;
        private static IAnimationService Animation => SimpleIoc.Default.GetInstance<IAnimationService>();

        public PhotoViewerView()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var animationObject = Animation.GetAnimation(PhotoViewModel.PhotoAnimationKey);
            if (animationObject != null)
            {
                var animation = animationObject as ConnectedAnimation;
                FullImage.ImageOpened += (sender, args) =>
                {
                    FullImage.Opacity = 1;
                    animation?.TryStart(FullImage);
                };
            }
            else
            {
                FullImage.Opacity = 1;
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            Animation.PrepareToAnimate(PhotoViewModel.PhotoAnimationKey, FullImage);
        }
    }
}
