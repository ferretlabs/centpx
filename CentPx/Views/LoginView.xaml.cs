﻿using CentPx.ViewModels;

namespace CentPx.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginView 
    {
        private LoginViewModel Login => DataContext as LoginViewModel;

        public LoginView()
        {
            InitializeComponent();
        }
    }
}
