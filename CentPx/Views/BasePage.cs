﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using CentPx.Core.Extensions;
using CentPx.ViewModels;
using Cimbalino.Toolkit.Controls;
using ViewModelBase = GalaSoft.MvvmLight.ViewModelBase;

namespace CentPx.Views
{
    public abstract class BasePage : ExtendedPageBase
    {
        private string _persistenceKey;

        protected BasePage()
        {
            NavigationCacheMode = NavigationCacheMode.Required;

            if (!ViewModelBase.IsInDesignModeStatic)
            {
                ApplicationView.GetForCurrentView().SetDesiredBoundsMode(ApplicationViewBoundsMode.UseCoreWindow);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (e.NavigationMode == NavigationMode.Back)
            {
                var listPage = this as IHasAList;
                if (listPage != null)
                {
                    if (e.NavigationMode == NavigationMode.Back && !string.IsNullOrEmpty(_persistenceKey))
                    {
                        ListViewPersistenceHelper.SetRelativeScrollPositionAsync(listPage.ListView, _persistenceKey, KeyToItemHandler).DontAwait("Just crack on with it");
                    }
                }
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            if (e.NavigationMode != NavigationMode.Back)
            {
                var listPage = this as IHasAList;
                if (listPage != null)
                {
                    _persistenceKey = ListViewPersistenceHelper.GetRelativeScrollPosition(listPage.ListView, ItemToKeyHandler);
                }
            }

            base.OnNavigatingFrom(e);
        }

        private static string ItemToKeyHandler(object item)
        {
            var photo = item as IHasId;
            return photo == null ? string.Empty : photo.Id;
        }

        private IAsyncOperation<object> KeyToItemHandler(string key)
        {
            Func<System.Threading.CancellationToken, Task<object>> taskProvider = token =>
            {
                var page = this as IHasAList;
                if (page == null) return Task.FromResult((object) null);
                var viewModel = page.GetList.Invoke();
                if (viewModel == null) return Task.FromResult((object)null);
                foreach (var item in viewModel)
                {
                    var id = item?.Id;
                    if (id == key)
                    {
                        return Task.FromResult((object)item);
                    }
                }

                return Task.FromResult((object)null);
            };

            return AsyncInfo.Run(taskProvider);
        }
    }
}
