﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using CentPx.ViewModels;

namespace CentPx.Views
{
    public interface IHasAList
    {
        ListViewBase ListView { get; }
        Func<IEnumerable<IHasId>> GetList { get; }
    }
}
