﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using CentPx.Services;
using CentPx.ViewModels;
using GalaSoft.MvvmLight.Ioc;

namespace CentPx.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HomeView : IHasAList
    {
        private HomeViewModel Home => DataContext as HomeViewModel;
        public ListViewBase ListView => ActivityList;
        private static IAnimationService Animation => SimpleIoc.Default.GetInstance<IAnimationService>();

        public Func<IEnumerable<IHasId>> GetList => () => Home.FriendActivityPhotos;

        public HomeView()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode == NavigationMode.Back)
            {
                
            }
        }
    }
}
