﻿<local:BasePage x:Class="CentPx.Views.UploadView"
                xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                xmlns:local="using:CentPx.Views"
                xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
                xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                xmlns:interactivity="using:Microsoft.Xaml.Interactivity"
                xmlns:behaviours="using:CentPx.Behaviours"
                xmlns:item="using:CentPx.ViewModels.Item"
                xmlns:data="using:CentPx.Data"
                xmlns:converters="using:Cimbalino.Toolkit.Converters"
                xmlns:controls="using:CompositionProToolkit.Controls"
                mc:Ignorable="d"
                DataContext="{Binding Upload, Source={StaticResource Locator}}">

    <Page.Resources>
        <converters:BooleanToBrushConverter x:Key="DroppingColourConverter"
                                            TrueValue="{ThemeResource SystemControlForegroundAccentBrush}"
                                            FalseValue="{ThemeResource NotDroppedBrush}" />

        <Style TargetType="TextBlock"
               x:Key="UploadHeaderTextBlockStyle"
               BasedOn="{StaticResource BodyTextBlockStyle}">
            <Setter Property="Foreground"
                    Value="{ThemeResource SystemControlForegroundAccentBrush}" />
        </Style>

        <DataTemplate x:Key="HeaderTemplate">
            <TextBlock Text="{Binding}"
                       Style="{StaticResource UploadHeaderTextBlockStyle}" />
        </DataTemplate>

        <data:DoubleProperty x:Key="ItemSize"
                             x:Name="ItemSize"
                             Value="300" />
    </Page.Resources>

    <Grid Background="{ThemeResource ApplicationPageBackgroundThemeBrush}">
        <VisualStateManager.VisualStateGroups>
            <VisualStateGroup x:Name="Widths">
                <VisualState x:Name="Narrow">
                    <VisualState.StateTriggers>
                        <AdaptiveTrigger MinWindowWidth="0" />
                    </VisualState.StateTriggers>
                    <VisualState.Setters>
                        <Setter Target="ItemSize.Value"
                                Value="150" />
                    </VisualState.Setters>
                </VisualState>
                <VisualState x:Name="Wide">
                    <VisualState.StateTriggers>
                        <AdaptiveTrigger MinWindowWidth="521" />
                    </VisualState.StateTriggers>
                    <VisualState.Setters>
                        <Setter Target="ItemSize.Value"
                                Value="300" />
                    </VisualState.Setters>
                </VisualState>
            </VisualStateGroup>
        </VisualStateManager.VisualStateGroups>
        <Grid x:Name="RootGrid"
              Background="{ThemeResource SecondaryBackgroundBrush}"
              BorderBrush="{x:Bind DropBehaviour.IsDropping, Mode=OneWay, Converter={StaticResource DroppingColourConverter}}"
              BorderThickness="2"
              Margin="15">
            <interactivity:Interaction.Behaviors>
                <behaviours:DropFileBehaviour DroppedCommand="{x:Bind Upload.DroppedFilesCommand}"
                                              x:Name="DropBehaviour" />
            </interactivity:Interaction.Behaviors>

            <RelativePanel Visibility="{x:Bind Upload.ShowFullPageBrowse, Mode=OneWay, Converter={StaticResource InverseBooleanToVisibilityConverter}}">
                <GridView ItemsSource="{x:Bind Upload.Files}"
                          SelectionMode="Multiple"
                          RelativePanel.AlignLeftWithPanel="True"
                          x:Name="Photos"
                          RelativePanel.LeftOf="PhotoDetails">
                    <interactivity:Interaction.Behaviors>
                        <behaviours:MultiSelectFilesBehavior SelectedItems="{x:Bind Upload.SelectedFiles, Mode=TwoWay}" />
                    </interactivity:Interaction.Behaviors>
                    <GridView.ItemTemplateSelector>
                        <data:UploadDataTemplateSelector AddItemContainerStyle="{StaticResource NoSelectionGridViewItemStyle}">
                            <data:UploadDataTemplateSelector.ImageItem>
                                <DataTemplate x:DataType="item:FileViewModel">
                                    <controls:ImageFrame Source="{x:Bind ImageUri, Mode=OneWay}"
                                                         Height="300"
                                                         Width="300"
                                                         Stretch="UniformToFill" />
                                </DataTemplate>
                            </data:UploadDataTemplateSelector.ImageItem>
                            <data:UploadDataTemplateSelector.AddItem>
                                <DataTemplate x:DataType="item:FileViewModel">
                                    <Grid Width="300"
                                          Height="300">
                                        <Button Style="{StaticResource EmptyButtonStyle}"
                                                VerticalAlignment="Stretch"
                                                VerticalContentAlignment="Stretch"
                                                Command="{Binding Upload.BrowseCommand, Source={StaticResource Locator}}">
                                            <StackPanel VerticalAlignment="Center"
                                                        HorizontalAlignment="Center">
                                                <SymbolIcon Symbol="Add"
                                                            HorizontalAlignment="Center"
                                                            Foreground="{ThemeResource NotDroppedBrush}" />
                                                <TextBlock Text="Add more photos"
                                                           HorizontalAlignment="Center"
                                                           Style="{StaticResource BodyTextBlockStyle}"
                                                           Foreground="{ThemeResource NotDroppedBrush}" />
                                            </StackPanel>
                                        </Button>
                                    </Grid>
                                </DataTemplate>
                            </data:UploadDataTemplateSelector.AddItem>
                        </data:UploadDataTemplateSelector>
                    </GridView.ItemTemplateSelector>
                </GridView>

                <RelativePanel RelativePanel.AlignRightWithPanel="True"
                               RelativePanel.AlignTopWithPanel="True"
                               RelativePanel.AlignBottomWithPanel="True"
                               x:Name="PhotoDetails"
                               Width="{x:Bind ItemSize.Value, Mode=OneWay}">
                    <Button Content="Publish"
                            HorizontalAlignment="Stretch"
                            RelativePanel.AlignTopWithPanel="True"
                            RelativePanel.AlignLeftWithPanel="True"
                            RelativePanel.AlignRightWithPanel="True"
                            Background="{ThemeResource PublishButtonBrush}"
                            BorderBrush="{ThemeResource PublishButtonBrush}"
                            Foreground="{ThemeResource SecondaryForegroundBrush}"
                            x:Name="Publish"
                            Margin="6,6,6,12"
                            Command="{x:Bind Upload.PublishPhotosCommand}"
                            IsEnabled="{x:Bind Upload.CanUpload, Mode=OneWay}" />
                    <ScrollViewer RelativePanel.AlignRightWithPanel="True"
                                  RelativePanel.AlignBottomWithPanel="True"
                                  RelativePanel.Below="Publish"
                                  Padding="6,0"
                                  VerticalScrollBarVisibility="Auto"
                                  Width="{x:Bind ItemSize.Value, Mode=OneWay}">
                        <RelativePanel HorizontalAlignment="Stretch">
                            <TextBlock Text="{x:Bind Upload.EditingLabel, Mode=OneWay}"
                                       Style="{StaticResource BodyTextBlockStyle}"
                                       x:Name="Editing"
                                       Margin="0,0,0,12"
                                       FontWeight="SemiBold" />

                            <TextBox x:Name="Keywords"
                                     Header="Keywords"
                                     Text="{Binding Keywords, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                                     IsEnabled="{x:Bind Upload.CanEdit, Mode=OneWay}"
                                     PlaceholderText="Adding at least 10 keywords helps your photo become more discoverable..."
                                     HeaderTemplate="{StaticResource HeaderTemplate}"
                                     TextWrapping="Wrap"
                                     RelativePanel.Below="Editing"
                                     RelativePanel.AlignRightWithPanel="True"
                                     RelativePanel.AlignLeftWithPanel="True"
                                     AcceptsReturn="True"
                                     Height="120"
                                     Margin="0,0,0,12" />

                            <Rectangle Height="1"
                                       Fill="Black"
                                       Opacity="0.4"
                                       x:Name="KeywordSeperator"
                                       Margin="0,12"
                                       RelativePanel.Below="Keywords"
                                       RelativePanel.AlignLeftWithPanel="True"
                                       RelativePanel.AlignRightWithPanel="True" />

                            <ComboBox x:Name="Category"
                                      IsEnabled="{x:Bind Upload.CanEdit, Mode=OneWay}"
                                      ItemsSource="{x:Bind Upload.CategoryTypes}"
                                      DisplayMemberPath="Value"
                                      SelectedItem="{Binding Category, Mode=TwoWay}"
                                      Header="Category"
                                      HeaderTemplate="{StaticResource HeaderTemplate}"
                                      Margin="0,0,0,12"
                                      RelativePanel.Below="KeywordSeperator"
                                      RelativePanel.AlignLeftWithPanel="True"
                                      RelativePanel.AlignRightWithPanel="True"
                                      HorizontalAlignment="Stretch" />

                            <Border RelativePanel.AlignBottomWith="Category"
                                    RelativePanel.AlignLeftWith="Category"
                                    RelativePanel.AlignRightWith="Category"
                                    Background="{ThemeResource SecondaryForegroundBrush}"
                                    Margin="6,0,30,18"
                                    Visibility="{x:Bind Upload.ShowMultiCategories, Mode=OneWay, FallbackValue=Collapsed}"
                                    IsHitTestVisible="False">
                                <TextBlock Text="{x:Bind Upload.MultiCategories}"
                                           Style="{StaticResource BodyTextBlockStyle}" />
                            </Border>

                            <TextBox HeaderTemplate="{StaticResource HeaderTemplate}"
                                     Header="Title"
                                     x:Name="Title"
                                     Text="{Binding Title, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                                     IsEnabled="{x:Bind Upload.CanEdit, Mode=OneWay}"
                                     Margin="0,0,0,12"
                                     RelativePanel.Below="Category"
                                     RelativePanel.AlignLeftWithPanel="True"
                                     RelativePanel.AlignRightWithPanel="True" />

                            <TextBlock Text="Adult content"
                                       Style="{StaticResource UploadHeaderTextBlockStyle}"
                                       HorizontalAlignment="Stretch"
                                       Margin="0,0,0,6"
                                       RelativePanel.Below="Title"
                                       RelativePanel.AlignLeftWithPanel="True"
                                       RelativePanel.AlignRightWithPanel="True"
                                       x:Name="AdultHeader" />
                            <CheckBox x:Name="Adult"
                                      IsEnabled="{x:Bind Upload.CanEdit, Mode=OneWay}"
                                      IsChecked="{x:Bind Upload.IsAdult, Mode=TwoWay}"
                                      Content="This photo contains nudity, sexually explicit or suggestive content"
                                      RelativePanel.Below="AdultHeader"
                                      RelativePanel.AlignLeftWithPanel="True"
                                      RelativePanel.AlignRightWithPanel="True"
                                      HorizontalAlignment="Stretch"
                                      Margin="0,0,0,12" />

                            <ComboBox x:Name="License"
                                      Header="License"
                                      IsEnabled="{x:Bind Upload.CanEdit, Mode=OneWay}"
                                      HeaderTemplate="{StaticResource HeaderTemplate}"
                                      RelativePanel.Below="Adult"
                                      RelativePanel.AlignLeftWithPanel="True"
                                      RelativePanel.AlignRightWithPanel="True"
                                      HorizontalAlignment="Stretch"
                                      ItemsSource="{x:Bind Upload.LicenseTypes}"
                                      DisplayMemberPath="Value"
                                      SelectedItem="{Binding License, Mode=TwoWay}"
                                      Margin="0,0,0,12" />

                            <Border RelativePanel.AlignBottomWith="License"
                                    RelativePanel.AlignLeftWith="License"
                                    RelativePanel.AlignRightWith="License"
                                    Background="{ThemeResource SecondaryBackgroundBrush}"
                                    Margin="6,0,30,18"
                                    Visibility="{x:Bind Upload.ShowMultiLicenses, Mode=OneWay, FallbackValue=Collapsed}"
                                    IsHitTestVisible="False">
                                <TextBlock Text="{x:Bind Upload.MultiLicenses}"
                                           Style="{StaticResource BodyTextBlockStyle}" />
                            </Border>

                            <TextBox x:Name="Description"
                                     HeaderTemplate="{StaticResource HeaderTemplate}"
                                     Header="Description"
                                     Text="{Binding Description, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                                     PlaceholderText="Tell us more about your beautiful photo"
                                     TextWrapping="Wrap"
                                     RelativePanel.Below="License"
                                     RelativePanel.AlignRightWithPanel="True"
                                     RelativePanel.AlignLeftWithPanel="True"
                                     AcceptsReturn="True"
                                     Height="120"
                                     Margin="0,0,0,12"
                                     IsEnabled="{x:Bind Upload.CanEdit, Mode=OneWay}" />

                            <TextBlock Text="Privacy"
                                       Style="{StaticResource UploadHeaderTextBlockStyle}"
                                       x:Name="PrivacyHeader"
                                       RelativePanel.Below="Description"
                                       RelativePanel.AlignLeftWithPanel="True"
                                       RelativePanel.AlignRightWithPanel="True"
                                       Margin="0,0,0,6" />
                            <CheckBox x:Name="Privacy"
                                      Content="Tick this if you want this photo to be private"
                                      RelativePanel.Below="PrivacyHeader"
                                      RelativePanel.AlignLeftWithPanel="True"
                                      RelativePanel.AlignRightWithPanel="True"
                                      HorizontalAlignment="Stretch"
                                      IsChecked="{x:Bind Upload.IsPrivate, Mode=TwoWay}"
                                      IsEnabled="{x:Bind Upload.CanEdit, Mode=OneWay}" />
                        </RelativePanel>
                    </ScrollViewer>
                </RelativePanel>
            </RelativePanel>

            <RelativePanel Visibility="{x:Bind Upload.ShowFullPageBrowse, Mode=OneWay, Converter={StaticResource BooleanToVisibilityConverter}, FallbackValue=Collapsed}"
                           HorizontalAlignment="Stretch"
                           VerticalAlignment="Stretch"
                           Background="Transparent">
                <TextBlock Text="Drag and drop photos into this window in order to begin adding them to 500px. Or click on the Browse button"
                           Style="{StaticResource SubtitleTextBlockStyle}"
                           Foreground="{x:Bind DropBehaviour.IsDropping, Mode=OneWay, Converter={StaticResource DroppingColourConverter}}"
                           x:Name="TextIncludingDrop"
                           RelativePanel.AlignHorizontalCenterWithPanel="True"
                           RelativePanel.AlignVerticalCenterWithPanel="True" />
                <TextBlock Text="Click the browse button in order to begin adding photos to 500px."
                           Style="{StaticResource SubtitleTextBlockStyle}"
                           Foreground="{x:Bind DropBehaviour.IsDropping, Mode=OneWay, Converter={StaticResource DroppingColourConverter}}"
                           x:Name="TextWithoutDrop"
                           Visibility="Collapsed"
                           RelativePanel.Below="TextIncludingDrop"
                           RelativePanel.AlignHorizontalCenterWith="TextIncludingDrop" />
                <Button Content="Browse"
                        RelativePanel.Below="TextWithoutDrop"
                        RelativePanel.AlignHorizontalCenterWith="TextWithoutDrop"
                        Background="Transparent"
                        Margin="0,12"
                        BorderBrush="{x:Bind DropBehaviour.IsDropping, Mode=OneWay, Converter={StaticResource DroppingColourConverter}}"
                        Command="{x:Bind Upload.BrowseCommand}" />
            </RelativePanel>

            <RelativePanel Background="#7F000000"
                           Visibility="{x:Bind Upload.IsUploading, FallbackValue=Visible}">
                <FontIcon Glyph="&#xE898;"
                          RelativePanel.AlignVerticalCenterWithPanel="True"
                          RelativePanel.AlignHorizontalCenterWithPanel="True"
                          Foreground="{ThemeResource SecondaryForegroundBrush}"
                          FontSize="72"
                          x:Name="UploadingIcon"/>
                <TextBlock Text="Uploading..."
                           RelativePanel.Below="UploadingIcon"
                           RelativePanel.AlignHorizontalCenterWith="UploadingIcon"
                           Foreground="{ThemeResource SecondaryForegroundBrush}"
                           Style="{StaticResource SubheaderTextBlockStyle}"/>
            </RelativePanel>
        </Grid>
    </Grid>
</local:BasePage>

