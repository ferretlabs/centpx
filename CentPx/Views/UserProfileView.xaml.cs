﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using CentPx.ViewModels;

namespace CentPx.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserProfileView : IHasAList
    {
        private UserProfileViewModel UserProfile => DataContext as UserProfileViewModel;

        public UserProfileView()
        {
            InitializeComponent();
        }

        public ListViewBase ListView => UserPhotos;
        public Func<IEnumerable<IHasId>> GetList => () => UserProfile.Photos;
    }
}
