﻿using CentPx.ViewModels.Item;

namespace CentPx.Controls.ItemControls
{
    public sealed partial class PhotoSmallListItem
    {
        private PhotoViewModel Photo => DataContext as PhotoViewModel;

        public PhotoSmallListItem()
        {
            InitializeComponent();

            DataContextChanged += (sender, args) => Bindings.Update();
        }
    }
}
