﻿using CentPx.ViewModels.Item;

namespace CentPx.Controls.ItemControls
{
    public sealed partial class PhotoListItem
    {
        private PhotoViewModel Photo => DataContext as PhotoViewModel;

        public PhotoListItem()
        {
            InitializeComponent();
            DataContextChanged += (sender, args) => Bindings.Update();
        }
    }
}
