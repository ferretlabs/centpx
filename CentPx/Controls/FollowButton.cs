﻿using System.Windows.Input;
using Windows.Devices.Enumeration;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace CentPx.Controls
{
    [TemplateVisualState(GroupName = FollowingGroup, Name = FollowingNormal)]
    [TemplateVisualState(GroupName = FollowingGroup, Name = FollowingPointerOver)]
    [TemplateVisualState(GroupName = FollowingGroup, Name = FollowingPressed)]
    [TemplateVisualState(GroupName = NotFollowingGroup, Name = NotFollowingNormal)]
    [TemplateVisualState(GroupName = NotFollowingGroup, Name = NotFollowingPointerOver)]
    [TemplateVisualState(GroupName = NotFollowingGroup, Name = NotFollowingPressed)]
    [TemplateVisualState(GroupName = BusyGroup, Name = IsBusyState)]
    [TemplateVisualState(GroupName = BusyGroup, Name = NotBusyState)]
    public class FollowButton : Control
    {
        private const string FollowingGroup = "Following";
        private const string NotFollowingGroup = "NotFollowing";
        private const string BusyGroup = "Busy";

        private const string PointerOver = "PointerOver";
        private const string Normal = "Normal";
        private const string Pressed = "Pressed";

        private const string FollowingPointerOver = FollowingGroup + PointerOver;
        private const string FollowingNormal = FollowingGroup + Normal;
        private const string FollowingPressed = FollowingGroup + Pressed;

        private const string NotFollowingPointerOver = NotFollowingGroup + PointerOver;
        private const string NotFollowingNormal = NotFollowingGroup + Normal;
        private const string NotFollowingPressed = NotFollowingGroup + Pressed;

        private const string IsBusyState = "IsBusy";
        private const string NotBusyState = "NotBusy";

        public static readonly DependencyProperty IsFollowingProperty = DependencyProperty.Register(
            nameof(IsFollowing), typeof(bool), typeof(FollowButton), new PropertyMetadata(default(bool), OnIsFollowingChanged));

        public bool IsFollowing
        {
            get { return (bool) GetValue(IsFollowingProperty); }
            set { SetValue(IsFollowingProperty, value); }
        }

        public static readonly DependencyProperty IsBusyProperty = DependencyProperty.Register(
            nameof(IsBusy), typeof(bool), typeof(FollowButton), new PropertyMetadata(default(bool), OnIsBusyChanged));

        public bool IsBusy
        {
            get { return (bool) GetValue(IsBusyProperty); }
            set { SetValue(IsBusyProperty, value); }
        }

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
            nameof(Command), typeof(ICommand), typeof(FollowButton), new PropertyMetadata(default(ICommand)));

        public ICommand Command
        {
            get { return (ICommand) GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public FollowButton()
        {
            DefaultStyleKey = typeof(FollowButton);
        }

        public static void OnIsFollowingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as FollowButton)?.SetPointerEnteredState();
            (sender as FollowButton)?.SetNormalState();
        }

        private static void OnIsBusyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as FollowButton)?.SetBusyState();
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            Tapped += (sender, args) =>
            {
                Command?.Execute(null);
            };

            SetNormalState();
        }

        protected override void OnPointerEntered(PointerRoutedEventArgs e)
        {
            base.OnPointerEntered(e);

            SetPointerEnteredState();
        }

        private void SetPointerEnteredState()
        {
            var state = IsFollowing ? FollowingPointerOver : NotFollowingPointerOver;
            VisualStateManager.GoToState(this, state, true);
        }

        protected override void OnPointerExited(PointerRoutedEventArgs e)
        {
            base.OnPointerExited(e);

            SetNormalState();
        }

        protected override void OnPointerPressed(PointerRoutedEventArgs e)
        {
            base.OnPointerPressed(e);
            SetPressedState();
        }

        private void SetBusyState()
        {
            var state = IsBusy ? IsBusyState : NotBusyState;
            VisualStateManager.GoToState(this, state, true);

            SetNormalState();
        }

        private void SetNormalState()
        {
            var state = IsFollowing ? FollowingNormal : NotFollowingNormal;
            VisualStateManager.GoToState(this, state, true);
        }

        private void SetPressedState()
        {
            var state = IsFollowing ? FollowingPressed: NotFollowingPressed;
            VisualStateManager.GoToState(this, state, true);
        }
    }
}
