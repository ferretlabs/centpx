﻿using CentPx.ViewModels.Item;

namespace CentPx.Controls
{
    public sealed partial class UserHeaderControl
    {
        private UserViewModel User => DataContext as UserViewModel;

        public UserHeaderControl()
        {
            InitializeComponent();
            DataContextChanged += (sender, args) => Bindings.Update();
        }
    }
}
