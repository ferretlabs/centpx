﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using CentPx.ViewModels.Item;

namespace CentPx.Controls
{
    [TemplateVisualState(GroupName = SizeGroup, Name = SmallState)]
    [TemplateVisualState(GroupName = SizeGroup, Name = MediumState)]
    [TemplateVisualState(GroupName = SizeGroup, Name = LargeState)]
    public sealed class UserBadge : Control
    {
        private const string SizeGroup = "BadgeSizes";
        private const string SmallState = "Small";
        private const string MediumState = "Medium";
        private const string LargeState = "Large";

        public static readonly DependencyProperty SizeProperty = DependencyProperty.Register(
            nameof(Size), typeof(BadgeSize), typeof(UserBadge), new PropertyMetadata(default(BadgeSize), (o, args) => (o as UserBadge)?.SetBadgeState()));

        public BadgeSize Size
        {
            get { return (BadgeSize)GetValue(SizeProperty); }
            set { SetValue(SizeProperty, value); }
        }

        public static readonly DependencyProperty UserProperty = DependencyProperty.Register(
            nameof(User), typeof(UserViewModel), typeof(UserBadge), new PropertyMetadata(default(UserViewModel)));

        public UserViewModel User
        {
            get { return (UserViewModel) GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public UserBadge()
        {
            DefaultStyleKey = typeof(UserBadge);
        }

        private void SetBadgeState()
        {
            string state;
            switch (Size)
            {
                case BadgeSize.Small:
                    state = SmallState;
                    break;
                case BadgeSize.Medium:
                    state = MediumState;
                    break;
                case BadgeSize.Large:
                    state = LargeState;
                    break;
                default:
                    state = string.Empty;
                    break;
            }

            if (string.IsNullOrEmpty(state))
            {
                return;
            }

            VisualStateManager.GoToState(this, state, true);
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetBadgeState();
        }
    }

    public enum BadgeSize
    {
        Small,
        Medium,
        Large
    }
}
