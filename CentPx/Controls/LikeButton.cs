﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace CentPx.Controls
{
    [TemplateVisualState(GroupName = LikedUnlikedGroup, Name = StateLiked)]
    [TemplateVisualState(GroupName = LikedUnlikedGroup, Name = StateUnliked)]
    public sealed class LikeButton : Button
    {
        private const string LikedUnlikedGroup = "LikedUnlikedGroup";
        private const string StateLiked = "Liked";
        private const string StateUnliked = "Unliked";

        public static readonly DependencyProperty IsLikedProperty = DependencyProperty.Register(
            nameof(IsLiked), typeof(bool), typeof(LikeButton), new PropertyMetadata(default(bool), IsLikedChanged));

        public bool IsLiked
        {
            get { return (bool) GetValue(IsLikedProperty); }
            set { SetValue(IsLikedProperty, value); }
        }

        public static readonly DependencyProperty LikesCountProperty = DependencyProperty.Register(
            nameof(LikesCount), typeof(string), typeof(LikeButton), new PropertyMetadata("0"));

        public string LikesCount
        {
            get { return (string) GetValue(LikesCountProperty); }
            set { SetValue(LikesCountProperty, value); }
        }

        public LikeButton()
        {
            DefaultStyleKey = typeof(LikeButton);
        }

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            SetColor();
        }

        private static void IsLikedChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as LikeButton)?.SetColor();
        }

        private void SetColor()
        {
            var color = IsLiked ? StateLiked : StateUnliked;
            VisualStateManager.GoToState(this, color, true);
        }
    }
}
