﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using CentPx.ViewModels.Item;

namespace CentPx.Data
{
    public class UploadDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate AddItem { get; set; }
        public DataTemplate ImageItem { get; set; }
        public Style AddItemContainerStyle { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var file = item as FileViewModel;
            if (file != null && file.IsDummyType)
            {
                var selector = container as SelectorItem;
                if (selector != null)
                {
                    //selector.IsEnabled = false;
                    selector.Style = AddItemContainerStyle;
                }

                return AddItem;
            }

            return ImageItem;
        }
    }
}
