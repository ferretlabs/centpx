﻿using Windows.UI.Xaml;

namespace CentPx.Data
{
    public class DoubleProperty : DependencyObject
    {
        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(nameof(Value), typeof(double), typeof(DoubleProperty),
                new PropertyMetadata(DependencyProperty.UnsetValue));
    }
}
