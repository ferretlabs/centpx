﻿using System;
using Windows.Foundation.Metadata;
using Windows.UI.Xaml;

namespace CentPx.Services
{
    public interface IAnimationService
    {
        void PrepareToAnimate(string name, object obj);
        object GetAnimation(string name);
    }

    public class AnimationService : IAnimationService
    {
        public void PrepareToAnimate(string name, object obj)
        {
            var element = obj as UIElement;
            if (element == null)
            {
                throw new ArgumentNullException(nameof(obj), "obj must be a UIElement");
            }

            if (!ApiInformation.IsApiContractPresent("Windows.Foundation.UniversalApiContract", 3))
            {
                return;
            }

            var service = Windows.UI.Xaml.Media.Animation.ConnectedAnimationService.GetForCurrentView();
            service.PrepareToAnimate(name, element);
        }

        public object GetAnimation(string name)
        {
            if (!ApiInformation.IsApiContractPresent("Windows.Foundation.UniversalApiContract", 3))
            {
                return null;
            }

            var service = Windows.UI.Xaml.Media.Animation.ConnectedAnimationService.GetForCurrentView();
            return service.GetAnimation(name);
        }
    }
}
