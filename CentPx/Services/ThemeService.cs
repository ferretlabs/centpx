﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CentPx.Services
{
    public interface IThemeService
    {
        ThemeItem GetTheme();
    }

    public class ThemeService : IThemeService
    {
        private readonly List<ThemeItem> _themes = new List<ThemeItem>
        {
            new ThemeItem {Name = "Midnight To Dawn", StartTime = TimeSpan.Zero, EndTime = new TimeSpan(5, 59, 59), GreetingText = "Some night photography?"},
            new ThemeItem {Name = "Sunrise To Early Morning", StartTime = TimeSpan.FromHours(6), EndTime = new TimeSpan(8, 59, 59), GreetingText = "Beautiful morning?"},
            new ThemeItem {Name = "Morning", StartTime = TimeSpan.FromHours(9), EndTime = new TimeSpan(11, 59, 59), GreetingText = "Good morning!"},
            new ThemeItem {Name = "Afternoon", StartTime = TimeSpan.FromHours(12), EndTime = new TimeSpan(16, 59, 59), GreetingText = "Good afternoon"},
            new ThemeItem {Name = "Early Evening To sunset", StartTime = TimeSpan.FromHours(17), EndTime = new TimeSpan(19, 59, 59), GreetingText = "Watching that sun set?"},
            new ThemeItem {Name = "Dusk", StartTime = TimeSpan.FromHours(20), EndTime = new TimeSpan(20, 59, 59), GreetingText = "Getting the most of the remaining light?"},
            new ThemeItem {Name = "Dusk To Midnight", StartTime = TimeSpan.FromHours(21), EndTime = new TimeSpan(23, 59, 59), GreetingText = "Time for some star trails?"}
        };

        public ThemeItem GetTheme()
        {
            var now = DateTime.Now;
            var theme = _themes.FirstOrDefault(x => now.TimeOfDay > x.StartTime && now.TimeOfDay <= x.EndTime);
            return theme ?? ThemeItem.DefaultTheme;
        }
    }

    public class ThemeItem
    {
        private const string ThemeUrl = "ms-appx:///Assets/ThemeBackgrounds/";
        public string Name { get; set; }
        public string BackgroundImage => $"{ThemeUrl}{Name.Replace(" ", string.Empty)}.jpg";
        public string GreetingText { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }

        public static ThemeItem DefaultTheme = new ThemeItem { GreetingText = "Welcome", Name = "Default theme" };
    }
}
