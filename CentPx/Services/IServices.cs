﻿using CentPx.Client;
using CentPx.Core.Services;
using Cimbalino.Toolkit.Services;

namespace CentPx.Services
{
    public interface IServices
    {
        IAuthenticationService Authentication { get; }
        INavigationService Navigation { get; }
        ICentPxClient CentPx { get; }
        IDispatcherService Dispatcher { get; }
        ILauncherService Launcher { get; }
        IAnimationService Animation { get; }
    }

    public class ServicesContainer : IServices
    {
        public ServicesContainer(
            IAuthenticationService authenticationService,
            INavigationService navigationService,
            ICentPxClient centPxClient,
            IDispatcherService dispatcher,
            ILauncherService launcher,
            IAnimationService animation)
        {
            Authentication = authenticationService;
            Navigation = navigationService;
            CentPx = centPxClient;
            Dispatcher = dispatcher;
            Launcher = launcher;
            Animation = animation;
        }

        public IAuthenticationService Authentication { get; }
        public INavigationService Navigation { get; }
        public ICentPxClient CentPx { get; }
        public IDispatcherService Dispatcher { get; }
        public ILauncherService Launcher { get; }
        public IAnimationService Animation { get; }
    }
}
