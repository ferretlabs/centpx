﻿using System.Threading;
using System.Threading.Tasks;
using CentPx.Client.Model.Requests;
using CentPx.Client.Model.Responses;

namespace CentPx.Collections
{
    public class UserProfilePhotoCollection : PhotoCollectionSource
    {
        public override Task<PhotosResponse> GetUserPhotos(int id, FetchPhotoOptions options, int page, CancellationToken cancellationToken = new CancellationToken())
        {
            return Services.CentPx.GetPhotosForUser(id, options, page, cancellationToken: cancellationToken);
        }
    }
}
