﻿using CentPx.Services;

namespace CentPx.Collections
{
    public class IncrementalLoadingCollectionWithId<TSource, TType> : ServiceBasedIncrementalLoadingCollection<TSource, TType>, INeedsAnId
        where TSource : IIncrementalSource<TType>, INeedsServices, INeedsAnId, new()
    {
        public IncrementalLoadingCollectionWithId(IServices services, int itemsPerPage = 20) 
            : base(services, itemsPerPage)
        {

        }

        public void SetId(int userId)
        {
            Source.SetId(userId);
        }
    }
}