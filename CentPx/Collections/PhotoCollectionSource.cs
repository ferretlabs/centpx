﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CentPx.Client.Model.Requests;
using CentPx.Client.Model.Responses;
using CentPx.Client.Model.Types;
using CentPx.ViewModels.Item;

namespace CentPx.Collections
{
    public abstract class PhotoCollectionSource : ServiceBasedCollectionSource, INeedsAnId, IIncrementalSource<PhotoViewModel>
    {
        private int _id;
        public void SetId(int id)
        {
            _id = id;
        }

        private PhotosResponse _lastFetchedPhotos;

        public bool HasMoreItems => _lastFetchedPhotos == null || (_lastFetchedPhotos.CurrentPage < _lastFetchedPhotos.TotalPages);

        public async Task<IEnumerable<PhotoViewModel>> GetPagedItems(int pageIndex, int pageSize)
        {
            if (Services == null)
            {
                return Enumerable.Empty<PhotoViewModel>();
            }

            if (pageIndex > _lastFetchedPhotos?.TotalPages)
            {
                return Enumerable.Empty<PhotoViewModel>();
            }

            var options = new FetchPhotoOptions
            {
                ImageSizes = new List<ImageSize> { ImageSize.Long2048, ImageSize.High600 },
                IncludeTags = true
            };

            var response = await GetUserPhotos(_id, options, pageIndex);

            var photos = response.Photos?.Select(x => new PhotoViewModel(x, Services)).ToList();

            response.Photos = null;
            _lastFetchedPhotos = response;
            
            return photos;
        }

        public abstract Task<PhotosResponse> GetUserPhotos(int id, FetchPhotoOptions options, int page, CancellationToken cancellationToken = default(CancellationToken));
    }
}