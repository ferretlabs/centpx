﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace CentPx.Collections
{
    public interface IIncrementalSource<T>
    {
        Task<IEnumerable<T>> GetPagedItems(int pageIndex, int pageSize);
        bool HasMoreItems { get; }
    }

    public class IncrementalLoadingCollection<TSource, TType> : ObservableCollection<TType>,
            ISupportIncrementalLoading
        where TSource : IIncrementalSource<TType>, new()
    {
        protected readonly TSource Source;
        private readonly int _itemsPerPage;
        private int _currentPage = 1;

        public IncrementalLoadingCollection(int itemsPerPage = 20)
        {
            Source = new TSource();
            _itemsPerPage = itemsPerPage;
        }

        public bool HasMoreItems => Source?.HasMoreItems ?? false;

        public TSource SourceCollection => Source;

        public IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync(uint count)
        {
            var dispatcher = Window.Current.Dispatcher;

            return Task.Run(
                async () =>
                {
                    uint resultCount = 0;
                    var result = await Source.GetPagedItems(_currentPage++, _itemsPerPage);

                    if (result == null || !result.Any())
                    {
                    }
                    else
                    {
                        resultCount = (uint)result.Count();

                        await dispatcher.RunAsync(
                            CoreDispatcherPriority.Normal,
                            () =>
                            {
                                foreach (TType item in result)
                                    Add(item);
                            });
                    }

                    return new LoadMoreItemsResult() { Count = resultCount };

                }).AsAsyncOperation();
        }
    }
}