﻿namespace CentPx.Collections
{
    public interface INeedsAnId
    {
        void SetId(int id);
    }
}