﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentPx.Client.Model;
using CentPx.Client.Model.Requests;
using CentPx.Client.Model.Responses;
using CentPx.ViewModels;

namespace CentPx.Collections
{
    public class SearchCollectionSource : ServiceBasedCollectionSource, IIncrementalSource<SearchResult>
    {
        private string _searchTerm;
        private SearchType _searchType;
        private CategoryTypes? _category;

        private UserSearchResponse _lastFetchedUserResponse;
        private PhotosResponse _lastFetchedPhotoResponse;

        public void SetSearchOptions(string searchTerm, SearchType searchType, CategoryTypes? category)
        {
            _lastFetchedUserResponse = null;
            _lastFetchedPhotoResponse = null;

            _searchTerm = searchTerm;
            _searchType = searchType;
            _category = category;
        }

        public async Task<IEnumerable<SearchResult>> GetPagedItems(int pageIndex, int pageSize)
        {
            if (Services == null || string.IsNullOrEmpty(_searchTerm))
            {
                return Enumerable.Empty<SearchResult>();
            }

            var task = _searchType == SearchType.Photos
                ? GetPhotos(pageIndex, pageSize)
                : GetUsers(pageIndex, pageSize);

            var results = await task;

            return results;
        }

        private async Task<List<SearchResult>> GetUsers(int pageIndex, int pageSize)
        {
            var response = await Services.CentPx.SearchUsers(_searchTerm, pageIndex, pageSize);
            _lastFetchedUserResponse = response;

            if (response.TotalItems == 0)
            {
                return new List<SearchResult>();
            }

            var users = response.Users.Select(x => new SearchResult(x, Services)).ToList();
            return users;
        }

        private async Task<List<SearchResult>> GetPhotos(int pageIndex, int pageSize)
        {
            SearchFetchPhotoOptions options = null;
            if (_category.HasValue)
            {
                options = new SearchFetchPhotoOptions
                {
                    Categories = new List<CategoryTypes> {_category.Value}
                };
            }

            var response = await Services.CentPx.SearchPhotos(_searchTerm, options, pageIndex, pageSize);
            if (response.TotalItems == 0)
            {
                return new List<SearchResult>();
            }

            var photos = response.Photos.Select(x => new SearchResult(x, Services)).ToList();
            return photos;
        }

        public bool HasMoreItems => SeeIfMoreItems();

        private bool SeeIfMoreItems()
        {
            if (Services == null)
            {
                return false;
            }

            if (_lastFetchedPhotoResponse == null && _lastFetchedUserResponse == null)
            {
                return true;
            }

            BaseItemsResponse response = null;

            if (_searchType == SearchType.Photos)
            {
                response = _lastFetchedUserResponse;
            }
            else if (_searchType == SearchType.Users)
            {
                response = _lastFetchedPhotoResponse;
            }

            if (response == null)
            {
                return true;
            }

            return response.CurrentPage < response.TotalPages;
        }
    }
}