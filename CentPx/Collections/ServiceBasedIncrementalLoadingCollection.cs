﻿using CentPx.Services;

namespace CentPx.Collections
{
    public class ServiceBasedIncrementalLoadingCollection<TSource, TType> : IncrementalLoadingCollection<TSource, TType>
        where TSource : IIncrementalSource<TType>, INeedsServices, new()
    {
        public ServiceBasedIncrementalLoadingCollection(IServices services, int itemsPerPage = 20)
            : base(itemsPerPage)
        {
            Source.SetServices(services);
        }
    }
}