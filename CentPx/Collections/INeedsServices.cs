﻿using CentPx.Services;

namespace CentPx.Collections
{
    public interface INeedsServices
    {
        void SetServices(IServices services);
    }
}