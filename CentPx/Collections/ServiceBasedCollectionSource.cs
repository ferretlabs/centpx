﻿using CentPx.Services;

namespace CentPx.Collections
{
    public abstract class ServiceBasedCollectionSource : INeedsServices
    {
        protected IServices Services;

        public void SetServices(IServices services)
        {
            Services = services;
        }
    }
}