﻿using System.Threading;
using System.Threading.Tasks;
using CentPx.Client.Model.Requests;
using CentPx.Client.Model.Responses;

namespace CentPx.Collections
{
    public class UserFollowingPhotoCollection : PhotoCollectionSource
    {
        public override Task<PhotosResponse> GetUserPhotos(int id, FetchPhotoOptions options, int page, CancellationToken cancellationToken = new CancellationToken())
        {
            return Services.CentPx.GetFriendsPhotos(id, options, page, cancellationToken: cancellationToken);
        }
    }
}